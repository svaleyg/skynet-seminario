# Estudio de factibilidad

El proyecto esta definido para ser ejecutado en un mes, esto debido a que es el tiempo limite que se tiene para la entrega de la solución. Dentro de los aspectos que tomaremos en cuenta son:

## Operativo
Dentro de los aspectos que se deben de considerar lo siguiente:
1. Disponibilidad de personal
2. Habilidades con las que debe de contar el personal
3. Capacidad para gestionar proyectos


### 1. Disponibilidad de personal
Se detalla a continuación el personal que debe de formar parte del equipo que ejecutará el proyecto:
* Gerente de proyecto
* Encargado del proyecto
* Líder técnico
* Desarrolladores
* Asegurador de calidad - QA
* Encargado de infraestructura

### 2. Habilidades con las que deben de contar el personal
Se detalla a continuación las habilidades con las que debe de contar el equipo:
* Todos deben de trabajar proyectos en base a metodologías agiles.
* El personal debe de tener ya experiencia previa en llevar proyectos de esta magnitud.
* El personal debe de tener al menos una certificación que avale que tienen las capacidades minimas para ejecutar su puesto.
* Cominicación fluida entre todos los integrantes del equipo.

### 3. Capacidad para gestionar proyectos
Dentro de la capacidad que debemos contar para ejecutar este proyecto, debemos considerar las siguientes:
* Todo el equipo debe de tener ya enmarcada la metodología de trabajo.
* Centralizar planificación, seguimiento y entregables en una herramienta de trabajo, en este caso Gitlab.
* Comunicación constante y fluida entre todo el equipo.
* Responsabilidad de todos los integrantes.

## Técnico
Para abordar y llevar a cabo la ejecución de este proyecto debemos de contar con lo siguiente:

1. Computadoras para todo el equipo
2. Licenciamiento de Gitlab para todo el equipo
3. Licenciamiento de Visual studio para los lideres técnicos y desarrolladores
4. Servidores de aplicación y de base de datos para el ambiente de desarrollo y para ambiente productivo
5. Los lideres técnios y desarrolladores deben de certificarse en Asp.Net Core y Web Apis
6. Los lideres técnios y desarrolladores deben de certificarse en base de datos PostgreSQL
7. El encargado del proyecto y gerente de proyecto debe de contar con su respectiva certificación
8. Capacitación en metodología agil

### 1. Computadoras para todo el equipo
Se debe de proporcionar este insumo a todos los integrantes del equipo, debido a que si no contarán con uno este puede afectar la comunicación entre todo el equipo.
### 2. Licenciamiento de Gitlab para todo el equipo
Se debe de tener licencias para los usuarios que integran el equipo, esto debido a que en el se centralizará toda la planificación, recolección de requerimientos, refinamientos, seguimiento de tareas de los integrantes, versionamiento de los códigos fuentes, entre otros detalles que pueden ser de utilidad.
### 3. Licenciamiento de Visual studio para los lideres técnicos y desarrolladores
Se debe de contar con licencias de Visual Studio para poder desarrollar las soluciones con las que contará el proyecto.
### 4. Servidores de aplicación y de base de datos para el ambiente de desarrollo y para ambiente productivo
Se debe de contar con un ambiente tanto de desarrollo como de producción. en el cual se estará desplegando continuamente todos los avances de los proyectos y los entregables.
### 5. Los lideres técnios y desarrolladores deben de certificarse en Asp.Net Core y Web Apis
Se debe de tener equipo de desarrollo que cuente con habilidades confirmadas en las tecnologías que se trabajaran en la solución propuesta.
### 6. Los lideres técnios y desarrolladores deben de certificarse en base de datos PostgreSQL
Se debe de tener equipo de desarrollo que cuente con habilidades confirmadas en las tecnologías que se trabajaran en la solución propuesta.
### 7. El encargado del proyecto y gerente de proyecto debe de contar con su respectiva certificación
Esto es necesario debido a que necesitamos un encargado del proyecto y gerente de proyecto que puedan ser capaz de gestionar el proyecto de una forma correcta.
### 8. Capacitación en metodología agil
Todo el equipo debe de recibir una capacitación en metodología agil, esto debido a que se debe de tener bien definido todo para no desalinear a todos en un inicio.

## Económico
Dentro de la inversión que se llevará a cabo por la empresa, se debe de considerar lo siguiente:

1. Planilla
2. Licenciamientos
3. Certificaciones
4. Recursos

#### 1. Planilla
|No. | Puesto | Sueldo Mensual
| :-: | - | -:
| 1 | Gerente de proyecto | Q 15,000.0
| 2 | Encargado del proyecto | Q 15,000.00
| 3 | Líder técnico | Q 12,000.00
| 4 | Desarrollador Back End 1 | Q 6,000.00
| 5 | Desarrollador Back End 2 | Q 6,000.00
| 6 | Desarrollador Front End 1 | Q 6,000.00
| 7 | Desarrollador Front End 2 | Q 6,000.00
| 8 | Desarrollador Senior | Q 10,000.00
| 9 | Asegurador de calidad - QA | Q 5,000.00
| 10 | Encargado de infraestructura | Q 8,000.00
| Total || Q 89,000.00
 
#### 2. Licenciamientos 
|No. | Tipo Licencia | Cantidad | Costo por Usuario | Total
| :-: | - | -: | -: | -:
| 1 | Gitlab Premium | 10 | Q 232.00 | Q 2,320.00
| 2 | Visual Studio Suscripción Profesional | 6 | Q 360.00 | Q 2,160.00
| Total | | | | Q 4,480.00

#### 3. Certificaciones
|No. | Curso | Cantidad | Costo por Usuario | Total
| :-: | - | -: | -: | -:
| 1 | ASP.NET CORE WEB API The Complete Guide | 6 | Q 559.92 | Q 3,359.52
| 2 | Project Management en Español - Gestion de Proyectos -35 hrs | 1 | Q 559.92 | Q 559.92
| 3 | Product Owner PSPO 1 Scrum Product Owner Certification 2023 | 1 | Q 599.92 | Q 599.92
| Total | | | | Q 4,519.36

#### 4. Recursos e insumos
|No. | Curso | Cantidad | Costo por Mes
| :-: | - | -: | -: 
| 1 | EC2 Windows Server t3.small | 1 | Q 226.00
| 2 | RDS for PostgreSQL db.t3.micro | 1 | Q 210.00
| Total | | | Q 436.00

### Totales
|No. | Puesto | Sueldo Mensual
| :-: | - | -:
| 1 | Planilla | Q 89,000.00
| 2 | Licenciamientos | Q 4,480.00
| 3 | Certificaciones | Q 4,519.36
| 4 | Recursos | Q 436.00
| Total || Q 98,435.36
