Requerimientos Skynet
=

Skynet ha solicitado que se le brinde una herramienta en la cual los clientes puedan reportar sus problemas con referencia a los servicios que se le brindan, para lo cual se requiere que el sistema principalmente pueda: ***Registrar casos de problemas que reporta el cliente, para posteriormente poder asignarselo a los técnicos que dan mantenimiento y soporte, como también poderles brindar una funcionalidad en la cual puedan llevar todo el seguimiento para cada visita progrmaada.***

Requerimientos del sistema
-

Según lo analizado en base a lo recolectado con el cliente y como también en base a la experiencia de poder brindar soluciones, se detalla la lista de requerimientos de nuestra propuesta.

### Requerimientos funcionales

El sistema debe de realizar lo que se describe en los requerimientos:
1. Gestionar agencias: Brindar funcionalidad en la cual se les permita gestionar las agencias de todo el país. Las acciones con las que contará dicha funcionalidades son:
    * Listar agencias
    * Crear agencias
    * Editar agencias
    * Habilitar o deshabilitar agencias
    * Eliminar agencias
2. Gestionar usuarios: Brindar funcionalidad para poder administrar los usuarios del sistema. Las acciones con las que contará son:
    * Listar usuarios
    * Crear usuarios
    * Editar usuarios
    * Habilitar o deshabilitar usuarios
    * Eliminar usuarios
    * Asignar encargado de usuario
3. Iniciar sesión a los sistemas: Brindar funcionalidad para que los usuarios puedan ingresar a travéz de su usuario y contraseña generada en el sistema. Tomar en consideración los siguientes puntos:
    * Iniciar sesión por una interfaz
    * Permitir el ingreso a las funcionalidades en base al rol asociado al usuario
4. Gestionar cartera de clientes: Brindar funcionalidad para poder administrar la cartera de cliente de la empresa. Las acciones con las que contará son:
    * Listar clientes
    * Crear clientes con su ubicación geográfica
    * Editar clientes con su ubicación geográfica
    * Habilitar o deshabilitar clientes
    * Eliminar clientes
5. Gestionar casos de quejas de servicio: Brindar funcionalidad para poder ingresar los casos al sistema. La acción con la que se contrará es:
    * Generár o registrar casos.
7. Gestionar casos y visitas: Brindar funcionalidades para poder realizar las siguientes tareas:
    * Administrar los casos para que el supervisor pueda poder realizarle las siguientes acciones:
        - Editar casos
        - Eliminar casos
        - Generar visitas y asignación de visita a usuario técnico
        - Cerrar casos
    * Realizar un seguimiento a las visitas asignadas a los técnicos: Brindar funcionalidad para poder realizar las siguientes acciones:
        - Iniciar visitas, se debe de capturar la hora en la que incia la visita, como también las coordenadas de donde se ubica el técnico
        - Ingresar tareas realizadas durante la visita
        - Finalizar visita, se debe de capturar la hora en la que indica que finalizó la visita
        - Enviar correo con el detalle de la visita al cliente
        - Anular visita, se debe de permitir realizar esta acción en caso no se lleve a cabo la visita
    * Generar tablero de visitas: se debe de proporcionar una funcionalidad para que los usuarios supervisores, puedan ver el detalle de las visitas que tienen asignadas los técnicos que tiene a su mando.


### Requerimientos no funcionales

El sistema debe de realizar lo que se describe a continuación:
1. Implementar sesiones de usuario en el sitio web:
    * Generar un manejador de sesiones
    * Generar los menú en base al rol del usuario
    * Mostrar foto de perfil del usario autenticado
3. Implementar la lógica de negocio en un servicio web rest.
2. Implementar autenticación por JWT Token en el servicio web rest.
3. Implementar ORM para transaccionar y consultar a la base de datos.
