# Migraciones a Base de Datos con .Net Core

## Crear una nueva migración

>```dotnet ef migrations add MigInicial```

## Aplicar el nuevo cambio de las migraciones
>```dotnet ef database update```

## Geolocalización Latitud y Longitudes para probar
15.10677501959598, -90.31550660853186
15.086527980969777, -90.48451018692275


# Docker
## Generando Imagen para desplegar en Docker
>```docker build -t seminario .```

## Iniciando servicio web en docker
>```docker run -d -p 5000:80 seminario```
