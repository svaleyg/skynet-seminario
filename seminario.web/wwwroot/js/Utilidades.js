﻿
function LlenarDataTable(url, divID, encabezados) {
    OpenWaitDialog();
    $.ajax({
        url: url,
        type: 'GET',
        success: function (result) {
            if (result != null && result != undefined) {
                GenerarTablaDinamica(divID, result, encabezados);
            }
        },
        error: function () {
            console.log("Fallo al generar la tabla dinámica");
            CloseWaitDialog()
        }
    });
}

var OpenWaitDialog = function () {
    $("#ModalLoading").show()
}

var CloseWaitDialog = function () {
    $("#ModalLoading").hide()
}

function GenerarEncabezados(divID, encabezados) {
    var idTabla = divID + 'Table';
    $('#' + divID).append('<table class="table table-striped table-hover display responsive nowrap" id="' + idTabla + '"></table >')

    var nombreTHead = idTabla + 'Head';
    $('#' + idTabla).append('<thead class= "thead-light"><tr id="' + nombreTHead + '" ></tr></thead>')

    var noColumnas = encabezados.length;
    var th = '';
    for (var i = 0; i < noColumnas; i++) {
        th = '<th class="text-center">' + encabezados[i] + '</th>';
        $("#" + nombreTHead).append(th);
    }

    var nombreTBody = idTabla + 'Body';
    $('#' + idTabla).append('<tbody id="' + nombreTBody + '" ></tbody>')
}

function GenerarTablaDinamica(divID, objeto, encabezados) {
    $('#' + divID).empty();
    GenerarEncabezados(divID, encabezados)

    var idTabla = divID + 'Table';
    var nombreTBody = idTabla + 'Body';
    var noColumnas = encabezados.length;
    var td = '';

    if (objeto.Result != undefined || objeto.Result != null) {
        Alerta(objeto.Title, objeto.Message, "warning")
    } else {
        if (objeto != null || objeto != undefined) {
            for (var i = 0; i < objeto.length; i += noColumnas) {
                td = '<tr>'
                for (var j = 0; j < noColumnas; j++) {
                    td += '<td>' + objeto[i + j] + '</td>';
                }
                td += '</tr>';
                $("#" + nombreTBody).append(td);
            }
        }
    }

    $('#' + idTabla).DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
        }
    });

    CloseWaitDialog();
}

function Alerta(titulo, mensaje, tipo) {
    $("#toastAlerta").removeClass();
    $("#toastAlerta").addClass("toast align-items-center text-white border-0 bg-" + tipo + " show");

    $("#toastAlertaTitulo").empty();
    $("#toastAlertaTitulo").append(titulo);

    $("#toastAlertaMensaje").empty();
    $("#toastAlertaMensaje").append(mensaje);
}