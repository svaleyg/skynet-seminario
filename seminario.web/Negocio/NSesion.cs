﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;
using System.Linq;

namespace seminario.web.Negocio
{
    public class NSesion
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NSesion(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        public async Task<string> ObtenerFuncionalidades()
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<Funcionalidad>>("Funcionalidades", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                return string.Empty;

            string html = string.Empty;

            if (result.Item1 != null)
            {
                List<Funcionalidad> funcionalidades = result.Item1;
                var padres = funcionalidades.Where(c => c.PadreID == null || c.PadreID == 0).ToList();

                if (padres != null)
                {
                    padres.ForEach(item =>
                    {
                        var hijos = funcionalidades.Where(c => c.PadreID == item.ID).ToList();

                        if (hijos != null && hijos.Count > 0)
                        {
                            html += $"<li class='nav-item dropdown'><a class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' role='button' data-bs-toggle='dropdown' aria-expanded='false'>{item.Nombre}</a><ul class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>";

                            hijos.ForEach(hijo =>
                            {
                                html += $"<li ><a class='dropdown-item' href='{(string.IsNullOrWhiteSpace(hijo.Ruta) ? "#" : hijo.Ruta)}'>{hijo.Nombre}</a></li>";
                            });

                            html += "</ul></li>";
                        }
                        else
                        {
                            html += $"<li class='nav-item'><a class='nav-link' href='{(string.IsNullOrWhiteSpace(item.Ruta) ? "#" : item.Ruta)}'>{item.Nombre}</a></li>";
                        }
                    });
                }
            }

            return html;
        }
    }
}
