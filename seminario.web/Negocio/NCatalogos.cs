﻿using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;

namespace seminario.web.Negocio
{
    public class NCatalogos
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NCatalogos(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        public async Task<List<T>> ObtenerCatalogos<T>(EnumCatalogos catalogo)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<T>>($"Catalogos/{Convert.ToInt32(catalogo)}", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

    }

    public enum EnumCatalogos
    {
        Categoria,
        EstadoCivil,
        EstadoVisita,
        Genero,
        Rol,
        TipoRegion,
        Region
    }
}
