﻿using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;

namespace seminario.web.Negocio
{
    public class NClientes
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NClientes(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        public async Task<List<Cliente>> ObtenerClientes()
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<Cliente>>("Clientes", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Cliente> ObtenerCliente(int id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Cliente>($"Clientes/{id}", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Result> EliminarCliente(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Clientes/{id}", null, TypeMethod.Delete, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> CambiarEstadoCliente(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Clientes/CambiarEstado/{id}", null, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> EditarCliente(Cliente cliente)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Cliente, Result>($"Clientes", cliente, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> AgregarCliente(Cliente cliente)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Cliente, Result>($"Clientes", cliente, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }
    }
}
