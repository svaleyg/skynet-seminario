﻿using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;

namespace seminario.web.Negocio
{
    public class NVisitas
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NVisitas(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        public async Task<List<Visita>> ObtenerVisitas()
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<Visita>>("Visitas", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Visita> ObtenerVisita(int id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Visita>($"Visitas/{id}", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Result> IniciarVisita(IniciarVisita request)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<IniciarVisita, Result>($"Visitas/Iniciar", request, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> AgregarTareasVisita(FinalizarVisita request)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<FinalizarVisita, Result>($"Visitas/AgregarTareasVisita", request, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> FinalizarVisita(FinalizarVisita request)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<FinalizarVisita, Result>($"Visitas/Finalizar", request, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> CancelarVisita(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Visitas/Cancelar/{id}", null, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> EliminarVisita(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Visitas/{id}", null, TypeMethod.Delete, token: _token);

            return result.Item1 ?? result.Item2;
        }
    }
}
