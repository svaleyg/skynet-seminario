﻿using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;

namespace seminario.web.Negocio
{
    public class NCasos
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NCasos(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        
        public async Task<List<Caso>> ObtenerCasos()
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<Caso>>("Casos", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Caso> ObtenerCaso(int id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Caso>($"Casos/{id}", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Result> EliminarCaso(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Casos/{id}", null, TypeMethod.Delete, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> CerrarCaso(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Casos/{id}", null, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> EditarCaso(Caso caso)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Caso, Result>($"Casos", caso, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> AgregarCaso(Caso caso)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Caso, Result>($"Casos", caso, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> CrearVisita(Visita visita)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Visita, Result>($"Casos/CrearVisita", visita, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }
    }
}
