﻿using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;

namespace seminario.web.Negocio
{
    public class NAgencias
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NAgencias(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        public async Task<List<Agencia>> ObtenerAgencias()
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<Agencia>>("Agencias", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Agencia> ObtenerAgencia(int id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Agencia>($"Agencias/{id}", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Result> EliminarAgencia(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Agencias/{id}", null, TypeMethod.Delete, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> CambiarEstadoAgencia(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Agencias/CambiarEstado/{id}", null, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> EditarAgencia(Agencia agencia)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Agencia, Result>($"Agencias", agencia, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> AgregarAgencia(Agencia agencia)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<Agencia, Result>($"Agencias", agencia, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }
    }
}
