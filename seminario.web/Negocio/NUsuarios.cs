﻿using Microsoft.Extensions.Options;
using NuGet.Common;
using seminario.web.Models;
using seminario.web.Utilidades;

namespace seminario.web.Negocio
{
    public class NUsuarios
    {
        private readonly ConexionServicioAPI _conexionServicioAPI;
        private readonly IOptions<AppSettings> _options;
        private readonly string _token;

        public NUsuarios(IOptions<AppSettings> options, IHttpContextAccessor context)
        {
            _options = options;
            _conexionServicioAPI = new(options.Value.ApiUrl);

            var user = context.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                var claims = context.HttpContext.User.Claims;
                _token = claims.FirstOrDefault(c => c.Type.Equals("Token"))?.Value;
            }
        }

        public async Task<Result> IniciarSesion(User usuario)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<User, UserCompleto>("Login", usuario, TypeMethod.Post, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                return result.Item2;

            return new Result()
            {
                Resultado = true,
                Mensaje = result.Item1.Token
            };
        }

        public async Task<List<UserCompleto>> ObtenerUsuarios()
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, List<UserCompleto>>("Usuarios", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<UserCompleto> ObtenerUsuario(int id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, UserCompleto>($"Usuarios/{id}", null, TypeMethod.Get, token: _token);

            if (result.Item2 != null && !result.Item2.Resultado)
                throw new Exception(result.Item2.Mensaje);

            return result.Item1;
        }

        public async Task<Result> EliminarUsuario(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Usuarios/{id}", null, TypeMethod.Delete, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> CambiarEstadoUsuario(string id)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<dynamic, Result>($"Usuarios/CambiarEstado/{id}", null, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> EditarUsuario(UserCompleto cliente)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<UserCompleto, Result>($"Usuarios", cliente, TypeMethod.Put, token: _token);

            return result.Item1 ?? result.Item2;
        }

        public async Task<Result> AgregarUsuario(UserCompleto cliente)
        {
            var result = await _conexionServicioAPI.EjecutarMetodoGenerico<UserCompleto, Result>($"Usuarios", cliente, TypeMethod.Post, token: _token);

            return result.Item1 ?? result.Item2;
        }
    }
}
