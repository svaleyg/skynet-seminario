﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using seminario.web.Models;
using seminario.web.Negocio;
using seminario.web.Utilidades;

namespace seminario.web.Controllers
{
    [TypeFilter(typeof(ValidarSesionFilter))]
    public class ClientesController : BaseController
    {
        private readonly NClientes _clientes;
        private readonly NCatalogos _catalogos;

        public ClientesController(NClientes clientes, NCatalogos catalogos, IHttpContextAccessor context, IOptions<AppSettings> options) : base(context, options)
        {
            _clientes = clientes;
            _catalogos = catalogos;
        }

        public async Task<IActionResult> Index()
        {
            List<Genero> generos = await _catalogos.ObtenerCatalogos<Genero>(EnumCatalogos.Genero);
            List<Region> regiones = await _catalogos.ObtenerCatalogos<Region>(EnumCatalogos.Region);
            List<EstadoCivil> estadosCiviles = await _catalogos.ObtenerCatalogos<EstadoCivil>(EnumCatalogos.EstadoCivil);

            ViewBag.Generos = generos ?? new List<Genero>();
            ViewBag.Departamentos = regiones?.Where(c => c.PadreID == null) ?? new List<Region>();
            regiones?.ForEach(c => c.Padre = null);
            ViewBag.Municipios = regiones?.Where(c => c.PadreID > 0) ?? new List<Region>();
            ViewBag.EstadosCiviles = estadosCiviles ?? new List<EstadoCivil>();

            return View();
        }


        [HttpGet]
        public async Task<JsonResult> ListaClientes()
        {
            try
            {
                List<Cliente> clientes = await _clientes.ObtenerClientes();

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                foreach (var item in clientes)
                {
                    itemString.Add(item.ID.ToString());
                    itemString.Add(item.Documento);
                    itemString.Add(item.Nombre);
                    itemString.Add(item.Apellido);
                    itemString.Add(item.FechaNacimiento.ToString("dd/MM/yyyy"));
                    itemString.Add(item.Direccion);
                    itemString.Add(item.Telefono);
                    itemString.Add(item.Estado ? "Activo" : "Inactivo");
                    string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";
                    botones += baseButton.Replace("@btn", "info")
                        .Replace("@tooltip", item.Estado ? "Inactivar" : "Activar")
                        .Replace("@fai", item.Estado ? "fa-eye" : "fa-eye-slash")
                        .Replace("@functionJS", $"CambiarEstadoCliente({item.ID})");
                    botones += baseButton.Replace("@btn", "warning")
                        .Replace("@tooltip", "Editar")
                        .Replace("@fai", "fa-pencil-square-o")
                        .Replace("@functionJS", $"ObtenerCliente({item.ID})");
                    botones += baseButton.Replace("@btn", "danger")
                        .Replace("@tooltip", "Eliminar")
                        .Replace("@fai", "fa-trash-o")
                        .Replace("@functionJS", $"EliminarCliente({item.ID})");
                    botones += "</div>";
                    itemString.Add(botones);
                }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CrearEditarClientes(Cliente cliente)
        {
            try
            {
                Result resultado;
                if (cliente.ID > 0)
                    resultado = await _clientes.EditarCliente(cliente);
                else
                    resultado = await _clientes.AgregarCliente(cliente);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> CambiarEstadoCliente(string id)
        {
            try
            {
                Result resultado = await _clientes.CambiarEstadoCliente(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> ObtenerCliente(int id)
        {
            try
            {
                Cliente clientes = await _clientes.ObtenerCliente(id);

                return Json(clientes);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> EliminarCliente(string id)
        {
            try
            {
                Result resultado = await _clientes.EliminarCliente(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }
    }
}
