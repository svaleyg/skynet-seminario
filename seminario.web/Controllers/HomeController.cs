﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Utilidades;
using System.Diagnostics;

namespace seminario.web.Controllers
{
    [TypeFilter(typeof(ValidarSesionFilter))]
    public class HomeController : BaseController
    {
        public HomeController(IHttpContextAccessor context, IOptions<AppSettings> options) : base(context, options)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}