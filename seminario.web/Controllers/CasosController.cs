﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using seminario.web.Models;
using seminario.web.Negocio;
using seminario.web.Utilidades;

namespace seminario.web.Controllers
{
    [TypeFilter(typeof(ValidarSesionFilter))]
    public class CasosController : BaseController
    {
        private readonly NCasos _casos;
        private readonly NCatalogos _catalogos;
        private readonly NClientes _clientes;
        private readonly NUsuarios _usuarios;
        private readonly NVisitas _visitas;
        private readonly IOptions<AppSettings> _options;

        public CasosController(NCasos casos, NCatalogos catalogos,
            NClientes clientes, NUsuarios usuarios, NVisitas visitas,
            IHttpContextAccessor context, IOptions<AppSettings> options)
            : base(context, options)
        {
            _casos = casos;
            _catalogos = catalogos;
            _clientes = clientes;
            _usuarios = usuarios;
            _visitas = visitas;
            _options = options;
        }

        #region Administración de Casos
        public async Task<IActionResult> Index()
        {
            try
            {
                List<Categoria> categorias = await _catalogos.ObtenerCatalogos<Categoria>(EnumCatalogos.Categoria);

                ViewBag.Categorias = categorias ?? new List<Categoria>();
            }
            catch
            {
            }

            return View();
        }

        [HttpGet]
        public async Task<JsonResult> ListaCasos()
        {
            try
            {
                List<Caso> casos = await _casos.ObtenerCasos();

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                foreach (var item in casos)
                {
                    itemString.Add(item.ID.ToString());
                    itemString.Add($"{item.Cliente.Nombre} {item.Cliente.Apellido}");
                    itemString.Add(item.CodigoSeguimiento);
                    itemString.Add(item.Observacion);
                    itemString.Add(item.Categoria.Nombre);
                    itemString.Add(item.Visitas?.Count().ToString());
                    itemString.Add(item.Cerrado ? "Cerrado" : "Abierto");

                    string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";
                    if (!item.Cerrado)
                    {
                        botones += baseButton.Replace("@btn", "warning")
                            .Replace("@tooltip", "Editar")
                            .Replace("@fai", "fa-pencil-square-o")
                            .Replace("@functionJS", $"ObtenerCaso({item.ID})");
                        botones += baseButton.Replace("@btn", "primary")
                            .Replace("@tooltip", "Generar Visita")
                            .Replace("@fai", "fa-plane")
                            .Replace("@functionJS", $"ProgramarVisita({item.ID})");
                        botones += baseButton.Replace("@btn", "secondary")
                            .Replace("@tooltip", "Ver Lista de Visita")
                            .Replace("@fai", "fa-globe")
                            .Replace("@functionJS", $"ListarVisitas({item.ID})");
                        botones += baseButton.Replace("@btn", "info")
                            .Replace("@tooltip", "Cerrar")
                            .Replace("@fai", "fa-times")
                            .Replace("@functionJS", $"CerrarCaso({item.ID})");
                        botones += baseButton.Replace("@btn", "danger")
                            .Replace("@tooltip", "Eliminar")
                            .Replace("@fai", "fa-trash-o")
                            .Replace("@functionJS", $"EliminarCaso({item.ID})");
                    }
                    botones += "</div>";

                    itemString.Add(botones);
                }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CrearEditarCasos(Caso caso)
        {
            try
            {
                Result resultado;
                if (caso.ID > 0)
                    resultado = await _casos.EditarCaso(caso);
                else
                    resultado = await _casos.AgregarCaso(caso);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> CerrarCaso(string id)
        {
            try
            {
                Result resultado = await _casos.CerrarCaso(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> ObtenerCaso(int id)
        {
            try
            {
                Caso casos = await _casos.ObtenerCaso(id);

                return Json(casos);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> EliminarCaso(string id)
        {
            try
            {
                Result resultado = await _casos.EliminarCaso(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }
        #endregion

        #region Registro de Casos
        public async Task<IActionResult> Registro()
        {
            try
            {
                List<Categoria> categorias = await _catalogos.ObtenerCatalogos<Categoria>(EnumCatalogos.Categoria);

                ViewBag.Categorias = categorias ?? new List<Categoria>();
            }
            catch
            {
            }

            return View();
        }

        [HttpGet]
        public async Task<JsonResult> ListaClientes()
        {
            try
            {
                List<Cliente> clientes = await _clientes.ObtenerClientes();

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                if (clientes != null && clientes.Count > 0)
                    foreach (var item in clientes.Where(c => c.Estado).ToList())
                    {
                        itemString.Add(item.ID.ToString());
                        itemString.Add(item.Documento);
                        itemString.Add(item.Nombre);
                        itemString.Add(item.Apellido);
                        itemString.Add(item.Telefono);
                        itemString.Add(item.Correo);
                        string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";
                        botones += baseButton.Replace("@btn", "info")
                            .Replace("@tooltip", "Seleccionar")
                            .Replace("@fai", "fa-hand-o-up")
                            .Replace("@functionJS", $"SeleccionarCliente({item.ID}, '{item.Nombre} {item.Apellido}')");
                        botones += "</div>";
                        itemString.Add(botones);
                    }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpGet]
        public async Task<JsonResult> ListaTecnicos()
        {
            try
            {
                List<UserCompleto> usuarios = await _usuarios.ObtenerUsuarios();

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                if (usuarios != null && usuarios.Count > 0)
                    foreach (var item in usuarios.Where(c => c.Estado && c.Rol.Nombre.Equals(_options.Value.RolNombre)).ToList())
                    {
                        itemString.Add(item.ID.ToString());
                        itemString.Add(item.Nombre);
                        itemString.Add(item.Correo);
                        itemString.Add($"<img src='data:image/png;base64, {item.FotoPerfil}' class='rounded img-thumbnail' width='80' height='80' alt='Not Found' />");
                        itemString.Add(item.Agencia.Direccion);
                        string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";
                        botones += baseButton.Replace("@btn", "info")
                            .Replace("@tooltip", "Seleccionar")
                            .Replace("@fai", "fa-hand-o-up")
                            .Replace("@functionJS", $"SeleccionarTecnico({item.ID}, '{item.Nombre}')");
                        botones += "</div>";
                        itemString.Add(botones);
                    }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CrearVisita(Visita visita)
        {
            try
            {
                Result resultado = await _casos.CrearVisita(visita);
                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }
        #endregion

        #region Seguimiento de Casos
        public async Task<IActionResult> Seguimiento(int? id)
        {
            TempData["VisitasTempID"] = id;

            return View();
        }

        [HttpGet]
        public async Task<JsonResult> ListaVisitas()
        {
            try
            {
                var id = TempData["VisitasTempID"];

                List<Visita> visitasCompletas = await _visitas.ObtenerVisitas();

                List<Visita> visitas;

                if (id != null)
                    visitas = visitasCompletas.Where(c => c.CasoID == Convert.ToInt32(id)).ToList();
                else
                    visitas = visitasCompletas;

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                foreach (var item in visitas)
                {
                    itemString.Add(item.ID.ToString());
                    itemString.Add($"{item.Usuario.Usuario}");
                    itemString.Add(item.Caso.CodigoSeguimiento);
                    itemString.Add(item.FechaVisita.ToString("dd/MM/yyyy"));

                    itemString.Add("<span class=\"fw-bold\">Inicio:</span> " + (item.FechaHoraInicio > DateTime.MinValue ?
                        item.FechaHoraInicio.ToString("dd/MM/yyyy HH:mm") : "-")
                        + "<br><span class=\"fw-bold\">Fin</span>: " + (item.FechaHoraFin > DateTime.MinValue ?
                        item.FechaHoraFin.ToString("dd/MM/yyyy HH:mm") : "-"));

                    itemString.Add($"{item.Latitud}<br>{item.Longitud}");
                    itemString.Add(item.EstadoVisita.Nombre);

                    string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";

                    if (!item.EstadoVisita.Nombre.Equals(_options.Value.EstadoCancelada)
                        && !item.EstadoVisita.Nombre.Equals(_options.Value.EstadoFinalizada))
                    {
                        if (!item.EstadoVisita.Nombre.Equals(_options.Value.EstadoIniciada)
                            && !item.EstadoVisita.Nombre.Equals(_options.Value.EstadoEnProceso))
                        {
                            botones += baseButton.Replace("@btn", "primary")
                                .Replace("@tooltip", "Iniciar Visita")
                                .Replace("@fai", "fa-play-circle-o")
                                .Replace("@functionJS", $"IniciarVisita({item.ID}, {item.Caso.Cliente.Latitud}, {item.Caso.Cliente.Longitud})");
                            botones += baseButton.Replace("@btn", "danger")
                                .Replace("@tooltip", "Eliminar")
                                .Replace("@fai", "fa-trash-o")
                                .Replace("@functionJS", $"EliminarVisita({item.ID})");
                        }
                        else
                        {
                            botones += baseButton.Replace("@btn", "info")
                                .Replace("@tooltip", "Agregar Tareas a la Visita")
                                .Replace("@fai", "fa-tasks")
                                .Replace("@functionJS", $"AgregarTareasVisita({item.ID})");
                            botones += baseButton.Replace("@btn", "success")
                                .Replace("@tooltip", "Finalizar Visita")
                                .Replace("@fai", "fa-stop-circle-o")
                                .Replace("@functionJS", $"FinalizarVisitas({item.ID})");
                        }
                    }

                    botones += "</div>";

                    itemString.Add(botones);
                }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpGet]
        public async Task<JsonResult> ObtenerVisita(int id)
        {
            try
            {
                Visita visita = await _visitas.ObtenerVisita(id);
                return Json(visita);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> IniciarVisita(IniciarVisita visita)
        {
            try
            {
                Result resultado = await _visitas.IniciarVisita(visita);
                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> FinalizarVisita(FinalizarVisita visita)
        {
            try
            {
                visita.Tareas = new();
                Result resultado = await _visitas.FinalizarVisita(visita);
                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> EliminarVisita(string id)
        {
            try
            {
                Result resultado = await _visitas.EliminarVisita(id);
                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> TareasVisita(FinalizarVisita visita)
        {
            try
            {

                visita.Tareas = visita.Tareas ?? new();
                Result resultado = await _visitas.AgregarTareasVisita(visita);
                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }
        #endregion
    }
}
