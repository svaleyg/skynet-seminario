﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Negocio;
using seminario.web.Utilidades;

namespace seminario.web.Controllers
{
    [TypeFilter(typeof(ValidarSesionFilter))]
    public class UsuariosController : BaseController
    {
        private readonly NUsuarios _usuarios;

        public UsuariosController(NUsuarios usuarios, IHttpContextAccessor context, IOptions<AppSettings> options) : base(context, options)
        {
            _usuarios = usuarios;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> ListaUsuarios()
        {
            try
            {
                List<UserCompleto> usuarios = await _usuarios.ObtenerUsuarios();

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                foreach (var item in usuarios)
                {
                    itemString.Add(item.ID.ToString());
                    itemString.Add(item.Usuario);
                    itemString.Add(item.Nombre);
                    itemString.Add(item.Correo);
                    itemString.Add($"<img src='data:image/png;base64, {item.FotoPerfil}' class='rounded img-thumbnail' width='80' height='80' alt='Not Found' />");
                    itemString.Add(item.Rol.Nombre);
                    itemString.Add($"{item.Agencia.Direccion}, {item.Agencia.Region.Nombre}");
                    itemString.Add(item.Estado ? "Activo" : "Inactivo");
                    string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";
                    botones += baseButton.Replace("@btn", "info")
                        .Replace("@tooltip", item.Estado ? "Inactivar" : "Activar")
                        .Replace("@fai", item.Estado ? "fa-eye" : "fa-eye-slash")
                        .Replace("@functionJS", $"CambiarEstadoUsuario({item.ID})");
                    botones += baseButton.Replace("@btn", "warning")
                        .Replace("@tooltip", "Editar")
                        .Replace("@fai", "fa-pencil-square-o")
                        .Replace("@functionJS", $"ObtenerUsuario({item.ID})");
                    botones += baseButton.Replace("@btn", "danger")
                        .Replace("@tooltip", "Eliminar")
                        .Replace("@fai", "fa-trash-o")
                        .Replace("@functionJS", $"EliminarUsuario({item.ID})");
                    botones += "</div>";
                    itemString.Add(botones);
                }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CrearEditarUsuarios(UserCompleto usuario)
        {
            try
            {
                Result resultado;
                if (usuario.ID > 0)
                    resultado = await _usuarios.EditarUsuario(usuario);
                else
                    resultado = await _usuarios.AgregarUsuario(usuario);

                return Json(new Result()
                {
                    Resultado = true,
                    Mensaje = "Exito"
                });
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> CambiarEstadoUsuario(string id)
        {
            try
            {
                Result resultado = await _usuarios.CambiarEstadoUsuario(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> ObtenerUsuario(int id)
        {
            try
            {
                UserCompleto usuario = await _usuarios.ObtenerUsuario(id);

                return Json(usuario);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> EliminarUsuario(string id)
        {
            try
            {
                Result resultado = await _usuarios.EliminarUsuario(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }
    }
}
