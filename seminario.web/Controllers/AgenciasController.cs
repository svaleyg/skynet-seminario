﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Negocio;
using seminario.web.Utilidades;

namespace seminario.web.Controllers
{
    [TypeFilter(typeof(ValidarSesionFilter))]
    public class AgenciasController : BaseController
    {
        private readonly NAgencias _agencias;

        public AgenciasController(NAgencias agencias, IHttpContextAccessor context, IOptions<AppSettings> options) : base(context, options)
        {
            _agencias = agencias;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<JsonResult> ListaAgencias()
        {
            try
            {
                List<Agencia> agencias = await _agencias.ObtenerAgencias();

                List<string> itemString = new List<string>();
                string baseButton = "<button type=\"button\" class=\"btn btn-outline-@btn\" data-bs-toggle=\"tooltip\" data-bs-placement=\"top\" title=\"@tooltip\" onclick=\"@functionJS\"><i class=\"fa @fai\" aria-hidden=\"true\"></i></button>";

                foreach (var item in agencias)
                {
                    itemString.Add(item.ID.ToString());
                    itemString.Add(item.Direccion);
                    itemString.Add($"{item.Region.Nombre}, {item.Region.Padre.Nombre}");
                    itemString.Add(item.Telefono);
                    itemString.Add(item.HorarioLunesViernes);
                    itemString.Add(item.HorarioSabado);
                    itemString.Add(item.HorarioDomingo);
                    itemString.Add(item.Estado ? "Activo" : "Inactivo");
                    string botones = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">";
                    botones += baseButton.Replace("@btn", "info")
                        .Replace("@tooltip", item.Estado ? "Inactivar" : "Activar")
                        .Replace("@fai", item.Estado ? "fa-eye" : "fa-eye-slash")
                        .Replace("@functionJS", $"CambiarEstadoAgencia({item.ID})");
                    botones += baseButton.Replace("@btn", "warning")
                        .Replace("@tooltip", "Editar")
                        .Replace("@fai", "fa-pencil-square-o")
                        .Replace("@functionJS", $"ObtenerAgencia({item.ID})");
                    botones += baseButton.Replace("@btn", "danger")
                        .Replace("@tooltip", "Eliminar")
                        .Replace("@fai", "fa-trash-o")
                        .Replace("@functionJS", $"EliminarAgencia({item.ID})");
                    botones += "</div>";
                    itemString.Add(botones);
                }

                return Json(itemString);
            }
            catch (Exception ex)
            {
                Result response = new()
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };
                return Json(response);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CrearEditarAgencias(Agencia agencia)
        {
            try
            {
                Result resultado;
                if (agencia.ID > 0)
                    resultado = await _agencias.EditarAgencia(agencia);
                else
                    resultado = await _agencias.AgregarAgencia(agencia);

                return Json(new Result()
                {
                    Resultado = true,
                    Mensaje = "Exito"
                });
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> CambiarEstadoAgencia(string id)
        {
            try
            {
                Result resultado = await _agencias.CambiarEstadoAgencia(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> ObtenerAgencia(int id)
        {
            try
            {
                Agencia agencias = await _agencias.ObtenerAgencia(id);

                return Json(agencias);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public async Task<JsonResult> EliminarAgencia(string id)
        {
            try
            {
                Result resultado = await _agencias.EliminarAgencia(id);

                return Json(resultado);
            }
            catch (Exception ex)
            {
                return Json(new Result() { Resultado = false, Mensaje = ex.Message });
            }
        }
    }
}
