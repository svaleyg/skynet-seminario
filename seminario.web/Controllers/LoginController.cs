﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Negocio;
using System.Security.Claims;

namespace seminario.web.Controllers
{
    public class LoginController : BaseController
    {
        private readonly NUsuarios _usuarios;

        public LoginController(NUsuarios usuarios, IHttpContextAccessor context, IOptions<AppSettings> options) : base(context, options)
        {
            _usuarios = usuarios;
        }

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            else
                return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(User usuario)
        {
            Result result = await _usuarios.IniciarSesion(usuario);

            if (result.Resultado)
            {
                List<Claim> claims = new()
                {
                    new Claim("Token", result.Mensaje)
                };

                ClaimsIdentity claimsIdentity = new(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                AuthenticationProperties properties = new()
                {
                    AllowRefresh = true
                };

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme
                    , new ClaimsPrincipal(claimsIdentity), properties);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Mensaje = result.Mensaje;
                return View();
            }
        }

        public async Task<IActionResult> CerrarSesion()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Login");
        }
    }
}
