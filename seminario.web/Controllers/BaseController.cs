﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Negocio;

namespace seminario.web.Controllers
{
    [Controller]
    public class BaseController : Controller
    {
        private readonly NSesion _sesion;
        private readonly IHttpContextAccessor _context;
        public BaseController(IHttpContextAccessor context, IOptions<AppSettings> option)
        {
            _context = context;
            _sesion = new(option, context);

            string funcionalidades = _sesion.ObtenerFuncionalidades().Result;

            if (!string.IsNullOrWhiteSpace(funcionalidades))
                context.HttpContext.Items["Menu"] = funcionalidades;
            else
                context.HttpContext.Items["Menu"] = string.Empty;
        }
    }
}
