﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using seminario.web.Models;
using seminario.web.Negocio;
using System.Security.Claims;

namespace seminario.web.Utilidades
{

    public class ValidarSesionFilter : IAuthorizationFilter
    {
        private readonly IOptions<AppSettings> _options;
        public ValidarSesionFilter(IOptions<AppSettings> options)
        {
            _options = options;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User;

            if (user != null && !user.Identity.IsAuthenticated)
                context.Result = new RedirectResult("/Login");
        }
    }
}
