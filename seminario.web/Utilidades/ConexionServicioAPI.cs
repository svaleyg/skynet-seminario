﻿using Newtonsoft.Json;
using seminario.web.Models;
using System.Net.Http.Headers;
using System.Text;

namespace seminario.web.Utilidades
{
    public class ConexionServicioAPI
    {
        private string _apiUrl;
        private bool _ejecutarMetodo;

        public ConexionServicioAPI(string apiUrl)
        {
            _apiUrl = apiUrl;
            if (string.IsNullOrEmpty(_apiUrl)) _ejecutarMetodo = false;
            else _ejecutarMetodo = true;
        }

        public async Task<Tuple<T, Result>> EjecutarMetodoGenerico<TIn, T>(string metodo, TIn contenido, TypeMethod tipoMetodo, string token = null)
        {
            if (!_ejecutarMetodo) throw new Exception("No se encontró la URL del API.");

            try
            {
                using (var cliente = new HttpClient())
                {
                    if (!string.IsNullOrEmpty(token))
                        cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    StringContent content = null;
                    if (contenido != null)
                        content = new StringContent(JsonConvert.SerializeObject(contenido), Encoding.UTF8, "application/json");

                    HttpResponseMessage respuesta;

                    switch (tipoMetodo)
                    {
                        case TypeMethod.Get:
                            respuesta = await cliente.GetAsync(string.Format("{0}{1}", _apiUrl, metodo));
                            break;
                        case TypeMethod.Post:
                            respuesta = await cliente.PostAsync(string.Format("{0}{1}", _apiUrl, metodo), content);
                            break;
                        case TypeMethod.Put:
                            respuesta = await cliente.PutAsync(string.Format("{0}{1}", _apiUrl, metodo), content);
                            break;
                        case TypeMethod.Delete:
                            respuesta = await cliente.DeleteAsync(string.Format("{0}{1}", _apiUrl, metodo));
                            break;
                        default:
                            throw new Exception("Método no implementado.");
                    }

                    if (respuesta.IsSuccessStatusCode)
                    {
                        string respuestaContenido = await respuesta.Content.ReadAsStringAsync();
                        try
                        {
                            T responseData = JsonConvert.DeserializeObject<T>(respuestaContenido);
                            return new Tuple<T, Result>(responseData, null);
                        }
                        catch
                        {
                            Result response = JsonConvert.DeserializeObject<Result>(respuestaContenido);
                            return new Tuple<T, Result>(default(T), response);
                        }
                    }
                    else
                    {
                        string respuestaContenido = await respuesta.Content.ReadAsStringAsync();

                        Result errorResponse = JsonConvert.DeserializeObject<Result>(respuestaContenido);

                        return new Tuple<T, Result>(default(T), errorResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                Result errorResponse = new Result
                {
                    Resultado = false,
                    Mensaje = ex.Message
                };

                return new Tuple<T, Result>(default(T), errorResponse);
            }
        }
    }

    public enum TypeMethod
    {
        Get,
        Post,
        Put,
        Delete
    }
}
