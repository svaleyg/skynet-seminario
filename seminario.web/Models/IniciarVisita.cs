﻿namespace seminario.web.Models
{
    public class IniciarVisita
    {
        public int ID { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
