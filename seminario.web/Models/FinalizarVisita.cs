﻿namespace seminario.web.Models
{
    public class FinalizarVisita
    {
        public int ID { get; set; }
        public List<string> Tareas { get; set; }
    }
}
