﻿namespace seminario.web.Models
{
    public class AppSettings
    {
        public string ApiUrl { get; set; }
        public string RolNombre { get; set; }
        public string EstadoAsignada { get; set; }
        public string EstadoIniciada { get; set; }
        public string EstadoEnProceso { get; set; }
        public string EstadoFinalizada { get; set; }
        public string EstadoCancelada { get; set; }
    }
}
