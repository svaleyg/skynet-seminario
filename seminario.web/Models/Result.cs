﻿namespace seminario.web.Models
{
    public class Result
    {
        public int ID { get; set; }
        public bool Resultado { get; set; }
        public string Mensaje { get; set; }
    }
}
