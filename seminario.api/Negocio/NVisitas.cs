﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NVisitas
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly ISesionUsuario _sesion;

        public NVisitas(ContextSeminario context, IOptions<AppSettings> appSettings, ISesionUsuario sesion)
        {
            _context = context;
            _appSettings = appSettings;
            _sesion = sesion;
        }

        public async Task<List<Visita>> ObtenerVisitas()
        {
            var visitas = await _context.Visitas
                .Include(c => c.Usuario)
                .Include(c => c.Caso)
                .ThenInclude(d => d.Cliente)
                .Include(c => c.EstadoVisita)
                .Where(c => !c.Eliminado).ToListAsync();

            if (visitas == null)
                throw new AppException("No se encuentraron visitas registradas", HttpStatusCode.NotFound);

            return visitas;
        }

        public async Task<Visita> ObtenerVisita(int id)
        {
            var visita = await _context.Visitas
                .Where(c => c.ID == id && !c.Eliminado)
                .Include(c => c.VisitaTareas).FirstOrDefaultAsync();

            if (visita == null)
                throw new AppException("No se encuentra la visita que buscas en nuestro sistema", HttpStatusCode.NotFound);

            return visita;
        }

        public async Task<OutputResultado> EliminarVisita(int id)
        {
            var visitaExist = await _context.Visitas.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (visitaExist == null)
                throw new AppException("Visita a eliminar no existe en el sistema", HttpStatusCode.BadRequest);

            visitaExist.Eliminado = true;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = visitaExist.ID,
                Resultado = true,
                Mensaje = "Visita eliminada satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> IniciarVisita(InputIniciarVisita request)
        {
            var visitaExist = await _context.Visitas.FirstOrDefaultAsync(c => c.ID == request.ID && !c.Eliminado);

            if (visitaExist == null)
                throw new AppException("Visita no existe en el sistema", HttpStatusCode.BadRequest);

            var estadoInicio = await _context.EstadoVisitas.FirstOrDefaultAsync(c => c.Nombre.ToUpper().Equals(_appSettings.Value.EstadoInicio.ToUpper()));

            if (estadoInicio == null)
                throw new AppException("No se encuentra registrado en base de datos el Estado Iniciada para iniciar la visita", HttpStatusCode.BadRequest);

            visitaExist.FechaHoraInicio = DateTime.Now;
            visitaExist.Latitud = request.Latitud;
            visitaExist.Longitud = request.Longitud;
            visitaExist.EstadoVisitaID = estadoInicio.ID;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = visitaExist.ID,
                Resultado = true,
                Mensaje = "Visita iniciada satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> AgregarTareasVisita(InputFinalizarVisita request)
        {
            OutputResultado result;
            using (var transaccion = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var visitaExist = await _context.Visitas.FirstOrDefaultAsync(c => c.ID == request.ID && !c.Eliminado);

                    if (visitaExist == null)
                        throw new AppException("Visita no existe en el sistema", HttpStatusCode.BadRequest);

                    if (request.Tareas == null || request.Tareas.Count == 0)
                        throw new AppException("Debe de ingresar las tareas realizadas para finalizar la visita", HttpStatusCode.BadRequest);

                    List<VisitaTarea> tareas = new();

                    request.Tareas.ForEach(item =>
                    {
                        tareas.Add(new()
                        {
                            VisitaID = visitaExist.ID,
                            Descripcion = item
                        });
                    });

                    await _context.AddRangeAsync(tareas);

                    var resultadoTareas = await _context.SaveChangesAsync();

                    result = new()
                    {
                        ID = visitaExist.ID,
                        Resultado = true,
                        Mensaje = "Tareas de la visita registradas satisfactoriamente"
                    };

                    await transaccion.CommitAsync();
                }
                catch (Exception ex)
                {
                    result = new()
                    {
                        ID = request.ID,
                        Resultado = false,
                        Mensaje = ex.Message
                    };

                    await transaccion.RollbackAsync();
                }
            }

            var visita = await _context.Visitas.Include(c => c.VisitaTareas).FirstOrDefaultAsync(c => c.ID == request.ID);

            Thread thread = new Thread(() => EnviarCorreoElectronico(visita));
            thread.Start();

            return result;
        }

        public async Task<OutputResultado> FinalizarVisita(InputFinalizarVisita request)
        {
            OutputResultado result;
            using (var transaccion = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var visitaExist = await _context.Visitas.FirstOrDefaultAsync(c => c.ID == request.ID && !c.Eliminado);

                    if (visitaExist == null)
                        throw new AppException("Visita no existe en el sistema", HttpStatusCode.BadRequest);

                    var estadoFin = await _context.EstadoVisitas.FirstOrDefaultAsync(c => c.Nombre.ToUpper().Equals(_appSettings.Value.EstadoFin.ToUpper()));

                    if (estadoFin == null)
                        throw new AppException("No se encuentra registrado en base de datos el Estado Finalizada para finalizar la visita", HttpStatusCode.BadRequest);

                    if (visitaExist.EstadoVisitaID == estadoFin.ID)
                        throw new AppException("Visita ya previamente finalizada", HttpStatusCode.BadRequest);

                    visitaExist.FechaHoraFin = DateTime.Now;
                    visitaExist.EstadoVisitaID = estadoFin.ID;

                    var resultado = await _context.SaveChangesAsync();

                    result = new()
                    {
                        ID = visitaExist.ID,
                        Resultado = true,
                        Mensaje = "Visita finalizada satisfactoriamente"
                    };

                    await transaccion.CommitAsync();
                }
                catch (Exception ex)
                {
                    result = new()
                    {
                        ID = request.ID,
                        Resultado = true,
                        Mensaje = ex.Message
                    };

                    await transaccion.RollbackAsync();
                }
            }

            var visita = await _context.Visitas.Include(c => c.VisitaTareas).FirstOrDefaultAsync(c => c.ID == request.ID);

            Thread thread = new Thread(() => EnviarCorreoElectronico(visita));
            thread.Start();

            return result;
        }

        public async Task<OutputResultado> CancelarVisita(int id)
        {
            var visitaExist = await _context.Visitas.Include(c => c.EstadoVisita)
                .FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (visitaExist == null)
                throw new AppException("Visita a cancelar no existe en el sistema", HttpStatusCode.BadRequest);

            if (!visitaExist.EstadoVisita.Nombre.ToUpper().Equals(_appSettings.Value.EstadoInicial.ToUpper()))
                throw new AppException("Visita a cancelar ya se encuentra en seguimiento o ya fue cancelada", HttpStatusCode.BadRequest);

            var estadoCancelado = await _context.EstadoVisitas.FirstOrDefaultAsync(c => c.Nombre.ToUpper().Equals(_appSettings.Value.EstadoCancelar.ToUpper()));

            if (estadoCancelado == null)
                throw new AppException("No se encuentra registrado en base de datos el Estado Cancelada para cancelar la visita", HttpStatusCode.BadRequest);

            visitaExist.EstadoVisitaID = estadoCancelado.ID;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = visitaExist.ID,
                Resultado = true,
                Mensaje = "Visita cancelada satisfactoriamente"
            };

            return result;
        }

        private void EnviarCorreoElectronico(Visita visita)
        {
            try
            {

            }
            catch (Exception ex) { }
        }
    }
}
