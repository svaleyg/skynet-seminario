﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NFuncionalidades
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly ISesionUsuario _sesion;

        public NFuncionalidades(ContextSeminario context, IOptions<AppSettings> appSettings, ISesionUsuario sesion)
        {
            _context = context;
            _appSettings = appSettings;
            _sesion = sesion;
        }

        public async Task<List<Funcionalidad>> ObtenerFuncionalidades()
        {
            var clientes = await _context.Usuarios
                .Include(c => c.Rol)
                .ThenInclude(c => c.RolFuncionalidades)
                .ThenInclude(c => c.Funcionalidad)
                .Where(c => c.ID == _sesion.ObtenerSesionUsuario().ID).FirstOrDefaultAsync();

            if (clientes == null)
                throw new AppException("No se encuentraron funcionalidades registradas", HttpStatusCode.NotFound);

            var result = clientes.Rol.RolFuncionalidades.Select(c => c.Funcionalidad).ToList();

            return result;
        }
    }
}
