﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NClientes
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly ISesionUsuario _sesion;

        public NClientes(ContextSeminario context, IOptions<AppSettings> appSettings, ISesionUsuario sesion)
        {
            _context = context;
            _appSettings = appSettings;
            _sesion = sesion;
        }

        public async Task<List<Cliente>> ObtenerClientes()
        {
            var clientes = await _context.Clientes.Where(c => !c.Eliminado).ToListAsync();

            if (clientes == null)
                throw new AppException("No se encuentraron clientes registrados", HttpStatusCode.NotFound);

            return clientes;
        }

        public async Task<Cliente> ObtenerCliente(int id)
        {
            var cliente = await _context.Clientes.Where(c => c.ID == id && !c.Eliminado).FirstOrDefaultAsync();

            if (cliente == null)
                throw new AppException("No se encuentra el cliente que buscas en nuestro sistema", HttpStatusCode.NotFound);

            return cliente;
        }

        public async Task<OutputResultado> CrearCliente(InputCliente request)
        {
            var clienteExist = await _context.Clientes.FirstOrDefaultAsync(c => c.Documento.ToUpper().Equals(request.Documento.ToUpper()) && !c.Eliminado);

            if (clienteExist != null)
                throw new AppException($"Cliente con el documento {request.Documento}, ya existe en el sistema", HttpStatusCode.BadRequest);

            Cliente cliente = new()
            {
                Documento = request.Documento,
                Nombre = request.Nombre,
                Apellido = request.Apellido,
                GeneroID = request.GeneroID,
                FechaNacimiento = request.FechaNacimiento,
                Correo = request.Correo,
                Telefono = request.Telefono,
                Direccion = request.Direccion,
                RegionID = request.RegionID,
                Ocupacion = request.Ocupacion,
                EstadoCivilID = request.EstadoCivilID,
                Latitud = request.Latitud,
                Longitud = request.Longitud,
                FechaCreacion = DateTime.Now,
                UsuarioCreacion = _sesion.ObtenerSesionUsuario()?.Usuario,
                Estado = true,
                Eliminado = false
            };

            await _context.AddAsync(cliente);
            var resultado = await _context.SaveChangesAsync();

            if (!(cliente.ID > 0))
                throw new AppException("Cliente no fue posible crearlo", HttpStatusCode.BadRequest);

            OutputResultado result = new()
            {
                ID = cliente.ID,
                Resultado = true,
                Mensaje = "Cliente creado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EditarCliente(InputCliente request)
        {
            var clienteExist = await _context.Clientes.FirstOrDefaultAsync(c => c.ID == request.ID && !c.Eliminado);

            if (clienteExist == null)
                throw new AppException("Cliente a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            clienteExist.Documento = request.Documento;
            clienteExist.Nombre = request.Nombre;
            clienteExist.Apellido = request.Apellido;
            clienteExist.GeneroID = request.GeneroID;
            clienteExist.FechaNacimiento = request.FechaNacimiento;
            clienteExist.Correo = request.Correo;
            clienteExist.Telefono = request.Telefono;
            clienteExist.Direccion = request.Direccion;
            clienteExist.RegionID = request.RegionID;
            clienteExist.Ocupacion = request.Ocupacion;
            clienteExist.EstadoCivilID = request.EstadoCivilID;
            clienteExist.Latitud = request.Latitud;
            clienteExist.Longitud = request.Longitud;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = clienteExist.ID,
                Resultado = true,
                Mensaje = "Cliente actualizado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EliminarCliente(int id)
        {
            var clienteExist = await _context.Clientes.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (clienteExist == null)
                throw new AppException("Cliente a eliminar no existe en el sistema", HttpStatusCode.BadRequest);

            clienteExist.Eliminado = true;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = clienteExist.ID,
                Resultado = true,
                Mensaje = "Cliente eliminado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> CambiarEstadoCliente(int id)
        {
            var clienteExist = await _context.Clientes.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (clienteExist == null)
                throw new AppException("Cliente a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            clienteExist.Estado = !clienteExist.Estado;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = clienteExist.ID,
                Resultado = true,
                Mensaje = $"Cliente {(clienteExist.Estado ? "Activado" : "Inactivado")} satisfactoriamente"
            };

            return result;
        }
    }
}
