﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NCatalogos
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;

        public NCatalogos(ContextSeminario context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings;
        }

        public async Task<object> ObtenerCatalogos(int catalogo)
        {
            switch ((EnumCatalogos)catalogo)
            {
                case EnumCatalogos.Categoria:
                    return await _context.Categorias.ToListAsync();
                case EnumCatalogos.EstadoCivil:
                    return await _context.EstadoCivil.ToListAsync();
                case EnumCatalogos.EstadoVisita:
                    return await _context.EstadoVisitas.ToListAsync();
                case EnumCatalogos.Genero:
                    return await _context.Generos.ToListAsync();
                case EnumCatalogos.Rol:
                    return await _context.Roles.ToListAsync();
                case EnumCatalogos.TipoRegion:
                    return await _context.TipoRegiones.ToListAsync();
                case EnumCatalogos.Region:
                    return await _context.Regiones.ToListAsync();
                default:
                    throw new AppException("Catalogo no implementado", HttpStatusCode.NotImplemented);
            }
        }
    }
}
