﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NUsuarios
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly GeneradorToken _generadorToken;

        public NUsuarios(ContextSeminario context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings;
            _generadorToken = new(appSettings);
        }

        public async Task<User> Login(string usuario, string contrasenia)
        {
            var user = await _context.Usuarios
                .Where(c => c.Usuario.ToUpper().Equals(usuario.ToUpper()) && c.Estado && !c.Eliminado)
                .Include(c => c.Agencia)
                .Include(c => c.Rol).ThenInclude(c => c.RolFuncionalidades).ThenInclude(c => c.Funcionalidad)
                .FirstOrDefaultAsync();

            if (user == null) throw new AppException("El usuario ingresado no existe en el sistema o esta bloqueado", HttpStatusCode.Unauthorized);

            if (!user.Contrasenia.Equals(contrasenia)) throw new AppException("Usuario o Contraseña incorrecta", HttpStatusCode.Unauthorized);

            user.Contrasenia = string.Empty;
            user.Token = _generadorToken.GenerarTokenJWT(user);
            return user;
        }

        public async Task<List<User>> ObtenerUsuarios()
        {
            var users = await _context.Usuarios
                .Include(c => c.Rol)
                .Include(c => c.Agencia).ThenInclude(c => c.Region)
                .Where(c => !c.Eliminado).ToListAsync();

            if (users == null)
                throw new AppException("No se encuentraron usuarios registrados", HttpStatusCode.NotFound);

            return users;
        }

        public async Task<User> ObtenerUsuario(int id)
        {
            var user = await _context.Usuarios.Where(c => c.ID == id && !c.Eliminado).FirstOrDefaultAsync();

            if (user == null)
                throw new AppException("No se encuentra el usuario que buscas en nuestro sistema", HttpStatusCode.NotFound);

            return user;
        }

        public async Task<OutputResultado> CrearUsuario(InputUsuarioCompleto request)
        {
            var userExist = await _context.Usuarios.FirstOrDefaultAsync(c => c.Usuario.ToUpper().Equals(request.Usuario.ToUpper()) && !c.Eliminado);

            if (userExist != null)
                throw new AppException("Usuario ya existe en el sistema", HttpStatusCode.BadRequest);

            User usuario = new()
            {
                Usuario = request.Usuario,
                Nombre = request.Nombre,
                Correo = request.Correo,
                Contrasenia = request.Contrasenia,
                FotoPerfil = request.FotoPerfil,
                FechaCreacion = DateTime.Now,
                RolID = request.RolID,
                AgenciaID = request.AgenciaID,
                Estado = true,
                Eliminado = false
            };

            await _context.AddAsync(usuario);
            var resultado = await _context.SaveChangesAsync();

            if (!(usuario.ID > 0))
                throw new AppException("Usuario no fue posible crearlo", HttpStatusCode.BadRequest);

            OutputResultado result = new()
            {
                ID = usuario.ID,
                Resultado = true,
                Mensaje = "Usuario creado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EditarUsuario(InputUsuarioCompleto request)
        {
            var userExist = await _context.Usuarios.FirstOrDefaultAsync(c => c.ID == request.ID && !c.Eliminado);

            if (userExist == null)
                throw new AppException("Usuario a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            userExist.Nombre = request.Nombre;
            userExist.Correo = request.Correo;
            userExist.Contrasenia = request.Contrasenia;
            userExist.FotoPerfil = request.FotoPerfil;
            userExist.RolID = request.RolID;
            userExist.AgenciaID = request.AgenciaID;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = userExist.ID,
                Resultado = true,
                Mensaje = "Usuario actualizado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EliminarUsuario(int id)
        {
            var userExist = await _context.Usuarios.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (userExist == null)
                throw new AppException("Usuario a eliminar no existe en el sistema", HttpStatusCode.BadRequest);

            userExist.Eliminado = true;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = userExist.ID,
                Resultado = true,
                Mensaje = "Usuario eliminado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> CambiarEstadoUsuario(int id)
        {
            var userExist = await _context.Usuarios.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (userExist == null)
                throw new AppException("Usuario a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            userExist.Estado = !userExist.Estado;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = userExist.ID,
                Resultado = true,
                Mensaje = $"Usuario {(userExist.Estado ? "Activado" : "Inactivado")} satisfactoriamente"
            };

            return result;
        }
    }
}
