﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NCasos
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly ISesionUsuario _sesion;

        public NCasos(ContextSeminario context, IOptions<AppSettings> appSettings, ISesionUsuario sesion)
        {
            _context = context;
            _appSettings = appSettings;
            _sesion = sesion;
        }

        public async Task<List<Caso>> ObtenerCasos()
        {
            var casos = await _context.Casos
                .Include(c => c.Cliente)
                .Include(c => c.Categoria)
                .Include(c => c.Visitas)
                .ToListAsync();

            if (casos == null)
                throw new AppException("No se encuentraron casos registrados", HttpStatusCode.NotFound);

            return casos;
        }

        public async Task<Caso> ObtenerCaso(int id)
        {
            var caso = await _context.Casos.Include(c => c.Cliente).Where(c => c.ID == id).FirstOrDefaultAsync();

            if (caso == null)
                throw new AppException("No se encuentra el caso que buscas en nuestro sistema", HttpStatusCode.NotFound);

            return caso;
        }

        public async Task<OutputResultado> CrearCaso(InputCaso request)
        {
            string codigoSeguimiento = Guid.NewGuid().ToString();
            var casoExist = await _context.Casos.FirstOrDefaultAsync(c => c.CodigoSeguimiento.ToUpper().Equals(codigoSeguimiento.ToUpper()));

            if (casoExist != null)
                throw new AppException($"Caso con el código de seguimiento {codigoSeguimiento}, ya existe en el sistema", HttpStatusCode.BadRequest);

            Caso caso = new()
            {
                ClienteID = request.ClienteID,
                CodigoSeguimiento = codigoSeguimiento,
                Observacion = request.Observacion,
                Cerrado = false,
                CategoriaID = request.CategoriaID,
                FechaCreacion = DateTime.Now
            };

            await _context.AddAsync(caso);
            var resultado = await _context.SaveChangesAsync();

            if (!(caso.ID > 0))
                throw new AppException("Caso no fue posible crearlo", HttpStatusCode.BadRequest);

            OutputResultado result = new()
            {
                ID = caso.ID,
                Resultado = true,
                Mensaje = $"Caso creado satisfactoriamente. Código de seguimiento {codigoSeguimiento}"
            };

            return result;
        }

        public async Task<OutputResultado> EditarCaso(InputCaso request)
        {
            var casoExist = await _context.Casos.FirstOrDefaultAsync(c => c.ID == request.ID);

            if (casoExist == null)
                throw new AppException("Caso a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            casoExist.ClienteID = request.ClienteID;
            casoExist.Observacion = request.Observacion;
            casoExist.CategoriaID = request.CategoriaID;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = casoExist.ID,
                Resultado = true,
                Mensaje = "Caso actualizado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EliminarCaso(int id)
        {
            var casoExist = await _context.Casos.FirstOrDefaultAsync(c => c.ID == id);

            if (casoExist == null)
                throw new AppException("Caso a eliminar no existe en el sistema", HttpStatusCode.BadRequest);

            if (casoExist.Cerrado)
                throw new AppException("Caso no puede ser eliminar ya que esta cerrado", HttpStatusCode.BadRequest);

            var visitaExist = await _context.Visitas.FirstOrDefaultAsync(c => c.CasoID == casoExist.ID);

            if (visitaExist != null)
                throw new AppException("Caso no puede eliminarse ya que se le programo una visita, proceda a Cancelar la visita", HttpStatusCode.BadRequest);

            _context.Remove(casoExist);
            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = casoExist.ID,
                Resultado = true,
                Mensaje = "Caso eliminado satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> CrearVisitaCaso(InputCrearVisita request)
        {
            var casoExist = await _context.Casos.FirstOrDefaultAsync(c => c.ID == request.CasoID);

            if (casoExist == null)
                throw new AppException("Caso no existe en el sistema", HttpStatusCode.BadRequest);

            if (casoExist.Cerrado)
                throw new AppException("Caso ya fue cerrado y no puede generarse visitas", HttpStatusCode.BadRequest);

            var estadoInicial = await _context.EstadoVisitas.FirstOrDefaultAsync(c => c.Nombre.ToUpper().Equals(_appSettings.Value.EstadoInicial.ToUpper()));

            if (estadoInicial == null)
                throw new AppException("No se encuentra registrado en base de datos el Estado Inicial para crear la visita", HttpStatusCode.BadRequest);

            Visita visita = new()
            {
                UsuarioID = request.UsuarioID,
                CasoID = request.CasoID,
                FechaVisita = request.FechaVisita,
                Latitud = string.Empty,
                Longitud = string.Empty,
                EstadoVisitaID = estadoInicial.ID,
                FechaCreacion = DateTime.Now,
                UsuarioCreacion = _sesion.ObtenerSesionUsuario()?.Usuario,
                Eliminado = false
            };

            await _context.AddAsync(visita);

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = visita.ID,
                Resultado = true,
                Mensaje = $"Visita programada satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> CerrarCaso(int id)
        {
            var casoExist = await _context.Casos.FirstOrDefaultAsync(c => c.ID == id && !c.Cerrado);

            if (casoExist == null)
                throw new AppException("Caso a cerrar no existe en el sistema", HttpStatusCode.BadRequest);

            casoExist.Cerrado = true;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = casoExist.ID,
                Resultado = true,
                Mensaje = "Caso cerrado satisfactoriamente"
            };

            return result;
        }
    }
}
