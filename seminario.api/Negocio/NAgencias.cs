﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using seminario.api.Modelo;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Net;

namespace seminario.api.Negocio
{
    public class NAgencias
    {
        private readonly ContextSeminario _context;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly ISesionUsuario _sesion;

        public NAgencias(ContextSeminario context, IOptions<AppSettings> appSettings, ISesionUsuario sesion)
        {
            _context = context;
            _appSettings = appSettings;
            _sesion = sesion;
        }

        public async Task<List<Agencia>> ObtenerAgencias()
        {
            var agencias = await _context.Agencias
                .Include(c => c.Region).ThenInclude(c => c.Padre)
                .Where(c => !c.Eliminado).ToListAsync();

            if (agencias == null)
                throw new AppException("No se encuentraron agencias registradas", HttpStatusCode.NotFound);

            return agencias;
        }

        public async Task<Agencia> ObtenerAgencia(int id)
        {
            var agencia = await _context.Agencias.Where(c => c.ID == id && !c.Eliminado).FirstOrDefaultAsync();

            if (agencia == null)
                throw new AppException("No se encuentra la agencia que buscas en nuestro sistema", HttpStatusCode.NotFound);

            return agencia;
        }

        public async Task<OutputResultado> CrearAgencia(InputAgencia request)
        {
            Agencia agencia = new()
            {
                Direccion = request.Direccion,
                RegionID = request.RegionID,
                Telefono = request.Telefono,
                HorarioLunesViernes = request.HorarioLunesViernes,
                HorarioSabado = request.HorarioSabado,
                HorarioDomingo = request.HorarioDomingo,
                Latitud = request.Latitud,
                Longitud = request.Longitud,
                Estado = true,
                Eliminado = false
            };

            await _context.AddAsync(agencia);
            var resultado = await _context.SaveChangesAsync();

            if (!(agencia.ID > 0))
                throw new AppException("Agencia no fue posible crearlo", HttpStatusCode.BadRequest);

            OutputResultado result = new()
            {
                ID = agencia.ID,
                Resultado = true,
                Mensaje = "Agencia creada satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EditarAgencia(InputAgencia request)
        {
            var agenciaExist = await _context.Agencias.FirstOrDefaultAsync(c => c.ID == request.ID && !c.Eliminado);

            if (agenciaExist == null)
                throw new AppException("Agencia a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            agenciaExist.Direccion = request.Direccion;
            agenciaExist.RegionID = request.RegionID;
            agenciaExist.Telefono = request.Telefono;
            agenciaExist.HorarioLunesViernes = request.HorarioLunesViernes;
            agenciaExist.HorarioSabado = request.HorarioSabado;
            agenciaExist.HorarioDomingo = request.HorarioDomingo;
            agenciaExist.Latitud = request.Latitud;
            agenciaExist.Longitud = request.Longitud;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = agenciaExist.ID,
                Resultado = true,
                Mensaje = "Agencia actualizada satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> EliminarAgencia(int id)
        {
            var agenciaExist = await _context.Agencias.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (agenciaExist == null)
                throw new AppException("Agencia a eliminar no existe en el sistema", HttpStatusCode.BadRequest);

            agenciaExist.Eliminado = true;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = agenciaExist.ID,
                Resultado = true,
                Mensaje = "Agencia eliminada satisfactoriamente"
            };

            return result;
        }

        public async Task<OutputResultado> CambiarEstadoAgencia(int id)
        {
            var agenciaExist = await _context.Agencias.FirstOrDefaultAsync(c => c.ID == id && !c.Eliminado);

            if (agenciaExist == null)
                throw new AppException("Agencia a modificar no existe en el sistema", HttpStatusCode.BadRequest);

            agenciaExist.Estado = !agenciaExist.Estado;

            var resultado = await _context.SaveChangesAsync();

            OutputResultado result = new()
            {
                ID = agenciaExist.ID,
                Resultado = true,
                Mensaje = $"Agencia {(agenciaExist.Estado ? "Activada" : "Inactivada")} satisfactoriamente"
            };

            return result;
        }
    }
}
