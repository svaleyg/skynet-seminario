﻿namespace seminario.api.Modelo
{
    public class InputIniciarVisita
    {
        public int ID { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
