﻿namespace seminario.api.Modelo
{
    public class InputCrearVisita
    {
        public int UsuarioID { get; set; }
        public int CasoID { get; set; }
        public DateTime FechaVisita { get; set; }
    }
}
