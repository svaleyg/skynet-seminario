﻿namespace seminario.api.Modelo
{
    public class InputAgencia
    {
        public int ID { get; set; }
        public string Direccion { get; set; }
        public int RegionID { get; set; }
        public string Telefono { get; set; }
        public string HorarioLunesViernes { get; set; }
        public string HorarioSabado { get; set; }
        public string HorarioDomingo { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
