﻿namespace seminario.api.Modelo
{
    public class InputFinalizarVisita
    {
        public int ID { get; set; }
        public List<string> Tareas { get; set; }
    }
}
