﻿namespace seminario.api.Modelo
{
    public class AppSettings
    {
        public JWT JWT { get; set; }
        public int TiempoExpiracion { get; set; }
        public string EstadoInicial { get; set; }
        public string EstadoInicio { get; set; }
        public string EstadoFin { get; set; }
        public string EstadoCancelar { get; set; }
    }

    public class JWT
    {
        public string ClaveSecreta { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
}
