﻿namespace seminario.api.Modelo
{
    public enum EnumCatalogos
    {
        Categoria,
        EstadoCivil,
        EstadoVisita,
        Genero,
        Rol,
        TipoRegion,
        Region
    }
}
