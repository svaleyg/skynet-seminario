﻿namespace seminario.api.Modelo
{
    public class OutputResultado
    {
        public int ID { get; set; }
        public bool Resultado { get; set; }
        public string Mensaje { get; set; }
    }
}
