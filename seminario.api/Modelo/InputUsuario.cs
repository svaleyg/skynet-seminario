﻿using System.ComponentModel.DataAnnotations.Schema;

namespace seminario.api.Modelo
{
    public class InputUsuario
    {
        public string? Usuario { get; set; }
        public string Contrasenia { get; set; }
    }

    public class InputUsuarioCompleto : InputUsuario
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string FotoPerfil { get; set; }
        public int RolID { get; set; }
        public int AgenciaID { get; set; }
    }
}
