﻿using seminario.api.Negocio;
using System.ComponentModel.DataAnnotations.Schema;

namespace seminario.api.Modelo
{
    public class Rol
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<RolFuncionalidad> RolFuncionalidades { get; set; }
        public List<User> Usuarios { get; set; }
    }

    public class Funcionalidad
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
        public string Ruta { get; set; }
        public Funcionalidad Padre { get; set; }
        public int? PadreID { get; set; }
        public List<Funcionalidad> Funcionalidades { get; set; }
        public List<RolFuncionalidad> RolFuncionalidades { get; set; }
    }

    public class RolFuncionalidad
    {
        public int ID { get; set; }
        public Rol Rol { get; set; }
        public int RolID { get; set; }
        public Funcionalidad Funcionalidad { get; set; }
        public int FuncionalidadID { get; set; }
    }

    public class User
    {
        public int ID { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Contrasenia { get; set; }
        public string FotoPerfil { get; set; }
        [NotMapped]
        public string Token { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Rol Rol { get; set; }
        public int RolID { get; set; }
        public Agencia Agencia { get; set; }
        public int AgenciaID { get; set; }
        public bool Estado { get; set; }
        public bool Eliminado { get; set; }
        public List<Visita> Visitas { get; set; }
    }

    public class Agencia
    {
        public int ID { get; set; }
        public string Direccion { get; set; }
        public Region Region { get; set; }
        public int RegionID { get; set; }
        public string Telefono { get; set; }
        public string HorarioLunesViernes { get; set; }
        public string HorarioSabado { get; set; }
        public string HorarioDomingo { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public bool Estado { get; set; }
        public bool Eliminado { get; set; }
        public List<User> Usuarios { get; set; }
    }

    public class Region
    {
        public int ID { get; set; }
        public TipoRegion TipoRegion { get; set; }
        public int TipoRegionID { get; set; }
        public string Nombre { get; set; }
        public Region Padre { get; set; }
        public int? PadreID { get; set; }
        public List<Region> Regiones { get; set; }
        public List<Agencia> Agencias { get; set; }
        public List<Cliente> Clientes { get; set; }
    }

    public class TipoRegion
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<Region> Regiones { get; set; }
    }

    public class Cliente
    {
        public int ID { get; set; }
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public Genero Genero { get; set; }
        public int GeneroID { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public Region Region { get; set; }
        public int RegionID { get; set; }
        public string Ocupacion { get; set; }
        public EstadoCivil EstadoCivil { get; set; }
        public int EstadoCivilID { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public bool Estado { get; set; }
        public bool Eliminado { get; set; }
        public List<Caso> Casos { get; set; }
    }

    public class Genero
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<Cliente> Clientes { get; set; }
    }

    public class EstadoCivil
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<Cliente> Clientes { get; set; }
    }

    public class Caso
    {
        public int ID { get; set; }
        public Cliente Cliente { get; set;}
        public int ClienteID { get; set;}
        public string CodigoSeguimiento { get; set; }
        public string Observacion { get; set; }
        public bool Cerrado { get; set; }
        public Categoria Categoria { get; set; }
        public int CategoriaID { get; set; }
        public DateTime FechaCreacion { get; set; }
        public List<Visita> Visitas { get; set; }
    }

    public class Categoria
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<Caso> Casos { get; set; }
    }

    public class Visita
    {
        public int ID { get; set; }
        public User Usuario { get; set; }
        public int UsuarioID { get; set; }
        public Caso Caso { get; set; }
        public int CasoID { get; set; }
        public DateTime FechaVisita { get; set; }
        public DateTime FechaHoraInicio { get; set; }
        public DateTime FechaHoraFin { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public EstadoVisita EstadoVisita { get; set; }
        public int EstadoVisitaID { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public bool Eliminado { get; set; }
        public List<VisitaTarea> VisitaTareas { get; set; }
    }

    public class EstadoVisita
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<Visita> Visitas { get; set; }
    }

    public class VisitaTarea
    {
        public int ID { get; set; }
        public Visita Visita { get; set; }
        public int VisitaID { get; set; }
        public string Descripcion { get; set; }
    }
}
