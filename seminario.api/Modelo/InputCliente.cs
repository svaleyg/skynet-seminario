﻿namespace seminario.api.Modelo
{
    public class InputCliente
    {
        public int ID { get; set; }
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int GeneroID { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public int RegionID { get; set; }
        public string Ocupacion { get; set; }
        public int EstadoCivilID { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
