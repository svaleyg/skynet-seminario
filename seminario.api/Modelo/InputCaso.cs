﻿namespace seminario.api.Modelo
{
    public class InputCaso
    {
        public int ID { get; set; }
        public int ClienteID { get; set; }
        public string Observacion { get; set; }
        public int CategoriaID { get; set; }
    }
}
