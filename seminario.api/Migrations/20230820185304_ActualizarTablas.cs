﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace seminario.api.Migrations
{
    public partial class ActualizarTablas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agencia_Region_RegionID",
                table: "Agencia");

            migrationBuilder.DropForeignKey(
                name: "FK_Caso_Categoria_CategoriaID",
                table: "Caso");

            migrationBuilder.DropForeignKey(
                name: "FK_Caso_Cliente_ClienteID",
                table: "Caso");

            migrationBuilder.DropForeignKey(
                name: "FK_Cliente_EstadoCivil_EstadoCivilID",
                table: "Cliente");

            migrationBuilder.DropForeignKey(
                name: "FK_Cliente_Genero_GeneroID",
                table: "Cliente");

            migrationBuilder.DropForeignKey(
                name: "FK_Cliente_Region_RegionID",
                table: "Cliente");

            migrationBuilder.DropForeignKey(
                name: "FK_Region_Region_PadreID",
                table: "Region");

            migrationBuilder.DropForeignKey(
                name: "FK_Region_TipoRegion_TipoRegionID",
                table: "Region");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuarios_Agencia_AgenciaID",
                table: "Usuarios");

            migrationBuilder.DropForeignKey(
                name: "FK_Visita_Caso_CasoID",
                table: "Visita");

            migrationBuilder.DropForeignKey(
                name: "FK_Visita_EstadoVisita_EstadoVisitaID",
                table: "Visita");

            migrationBuilder.DropForeignKey(
                name: "FK_Visita_Usuarios_UsuarioID",
                table: "Visita");

            migrationBuilder.DropForeignKey(
                name: "FK_VisitaTarea_Visita_VisitaID",
                table: "VisitaTarea");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VisitaTarea",
                table: "VisitaTarea");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Visita",
                table: "Visita");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipoRegion",
                table: "TipoRegion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Region",
                table: "Region");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Genero",
                table: "Genero");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EstadoVisita",
                table: "EstadoVisita");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cliente",
                table: "Cliente");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Categoria",
                table: "Categoria");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Caso",
                table: "Caso");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Agencia",
                table: "Agencia");

            migrationBuilder.RenameTable(
                name: "VisitaTarea",
                newName: "VisitaTareas");

            migrationBuilder.RenameTable(
                name: "Visita",
                newName: "Visitas");

            migrationBuilder.RenameTable(
                name: "TipoRegion",
                newName: "TipoRegiones");

            migrationBuilder.RenameTable(
                name: "Region",
                newName: "Regiones");

            migrationBuilder.RenameTable(
                name: "Genero",
                newName: "Generos");

            migrationBuilder.RenameTable(
                name: "EstadoVisita",
                newName: "EstadoVisitas");

            migrationBuilder.RenameTable(
                name: "Cliente",
                newName: "Clientes");

            migrationBuilder.RenameTable(
                name: "Categoria",
                newName: "Categorias");

            migrationBuilder.RenameTable(
                name: "Caso",
                newName: "Casos");

            migrationBuilder.RenameTable(
                name: "Agencia",
                newName: "Agencias");

            migrationBuilder.RenameIndex(
                name: "IX_VisitaTarea_VisitaID",
                table: "VisitaTareas",
                newName: "IX_VisitaTareas_VisitaID");

            migrationBuilder.RenameIndex(
                name: "IX_Visita_UsuarioID",
                table: "Visitas",
                newName: "IX_Visitas_UsuarioID");

            migrationBuilder.RenameIndex(
                name: "IX_Visita_EstadoVisitaID",
                table: "Visitas",
                newName: "IX_Visitas_EstadoVisitaID");

            migrationBuilder.RenameIndex(
                name: "IX_Visita_CasoID",
                table: "Visitas",
                newName: "IX_Visitas_CasoID");

            migrationBuilder.RenameIndex(
                name: "IX_Region_TipoRegionID",
                table: "Regiones",
                newName: "IX_Regiones_TipoRegionID");

            migrationBuilder.RenameIndex(
                name: "IX_Region_PadreID",
                table: "Regiones",
                newName: "IX_Regiones_PadreID");

            migrationBuilder.RenameIndex(
                name: "IX_Cliente_RegionID",
                table: "Clientes",
                newName: "IX_Clientes_RegionID");

            migrationBuilder.RenameIndex(
                name: "IX_Cliente_GeneroID",
                table: "Clientes",
                newName: "IX_Clientes_GeneroID");

            migrationBuilder.RenameIndex(
                name: "IX_Cliente_EstadoCivilID",
                table: "Clientes",
                newName: "IX_Clientes_EstadoCivilID");

            migrationBuilder.RenameIndex(
                name: "IX_Caso_ClienteID",
                table: "Casos",
                newName: "IX_Casos_ClienteID");

            migrationBuilder.RenameIndex(
                name: "IX_Caso_CategoriaID",
                table: "Casos",
                newName: "IX_Casos_CategoriaID");

            migrationBuilder.RenameIndex(
                name: "IX_Agencia_RegionID",
                table: "Agencias",
                newName: "IX_Agencias_RegionID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VisitaTareas",
                table: "VisitaTareas",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Visitas",
                table: "Visitas",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipoRegiones",
                table: "TipoRegiones",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Regiones",
                table: "Regiones",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Generos",
                table: "Generos",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EstadoVisitas",
                table: "EstadoVisitas",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Clientes",
                table: "Clientes",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Categorias",
                table: "Categorias",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Casos",
                table: "Casos",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Agencias",
                table: "Agencias",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Agencias_Regiones_RegionID",
                table: "Agencias",
                column: "RegionID",
                principalTable: "Regiones",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Casos_Categorias_CategoriaID",
                table: "Casos",
                column: "CategoriaID",
                principalTable: "Categorias",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Casos_Clientes_ClienteID",
                table: "Casos",
                column: "ClienteID",
                principalTable: "Clientes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Clientes_EstadoCivil_EstadoCivilID",
                table: "Clientes",
                column: "EstadoCivilID",
                principalTable: "EstadoCivil",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Clientes_Generos_GeneroID",
                table: "Clientes",
                column: "GeneroID",
                principalTable: "Generos",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Clientes_Regiones_RegionID",
                table: "Clientes",
                column: "RegionID",
                principalTable: "Regiones",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Regiones_Regiones_PadreID",
                table: "Regiones",
                column: "PadreID",
                principalTable: "Regiones",
                principalColumn: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Regiones_TipoRegiones_TipoRegionID",
                table: "Regiones",
                column: "TipoRegionID",
                principalTable: "TipoRegiones",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuarios_Agencias_AgenciaID",
                table: "Usuarios",
                column: "AgenciaID",
                principalTable: "Agencias",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visitas_Casos_CasoID",
                table: "Visitas",
                column: "CasoID",
                principalTable: "Casos",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visitas_EstadoVisitas_EstadoVisitaID",
                table: "Visitas",
                column: "EstadoVisitaID",
                principalTable: "EstadoVisitas",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visitas_Usuarios_UsuarioID",
                table: "Visitas",
                column: "UsuarioID",
                principalTable: "Usuarios",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VisitaTareas_Visitas_VisitaID",
                table: "VisitaTareas",
                column: "VisitaID",
                principalTable: "Visitas",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agencias_Regiones_RegionID",
                table: "Agencias");

            migrationBuilder.DropForeignKey(
                name: "FK_Casos_Categorias_CategoriaID",
                table: "Casos");

            migrationBuilder.DropForeignKey(
                name: "FK_Casos_Clientes_ClienteID",
                table: "Casos");

            migrationBuilder.DropForeignKey(
                name: "FK_Clientes_EstadoCivil_EstadoCivilID",
                table: "Clientes");

            migrationBuilder.DropForeignKey(
                name: "FK_Clientes_Generos_GeneroID",
                table: "Clientes");

            migrationBuilder.DropForeignKey(
                name: "FK_Clientes_Regiones_RegionID",
                table: "Clientes");

            migrationBuilder.DropForeignKey(
                name: "FK_Regiones_Regiones_PadreID",
                table: "Regiones");

            migrationBuilder.DropForeignKey(
                name: "FK_Regiones_TipoRegiones_TipoRegionID",
                table: "Regiones");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuarios_Agencias_AgenciaID",
                table: "Usuarios");

            migrationBuilder.DropForeignKey(
                name: "FK_Visitas_Casos_CasoID",
                table: "Visitas");

            migrationBuilder.DropForeignKey(
                name: "FK_Visitas_EstadoVisitas_EstadoVisitaID",
                table: "Visitas");

            migrationBuilder.DropForeignKey(
                name: "FK_Visitas_Usuarios_UsuarioID",
                table: "Visitas");

            migrationBuilder.DropForeignKey(
                name: "FK_VisitaTareas_Visitas_VisitaID",
                table: "VisitaTareas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VisitaTareas",
                table: "VisitaTareas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Visitas",
                table: "Visitas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipoRegiones",
                table: "TipoRegiones");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Regiones",
                table: "Regiones");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Generos",
                table: "Generos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EstadoVisitas",
                table: "EstadoVisitas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Clientes",
                table: "Clientes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Categorias",
                table: "Categorias");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Casos",
                table: "Casos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Agencias",
                table: "Agencias");

            migrationBuilder.RenameTable(
                name: "VisitaTareas",
                newName: "VisitaTarea");

            migrationBuilder.RenameTable(
                name: "Visitas",
                newName: "Visita");

            migrationBuilder.RenameTable(
                name: "TipoRegiones",
                newName: "TipoRegion");

            migrationBuilder.RenameTable(
                name: "Regiones",
                newName: "Region");

            migrationBuilder.RenameTable(
                name: "Generos",
                newName: "Genero");

            migrationBuilder.RenameTable(
                name: "EstadoVisitas",
                newName: "EstadoVisita");

            migrationBuilder.RenameTable(
                name: "Clientes",
                newName: "Cliente");

            migrationBuilder.RenameTable(
                name: "Categorias",
                newName: "Categoria");

            migrationBuilder.RenameTable(
                name: "Casos",
                newName: "Caso");

            migrationBuilder.RenameTable(
                name: "Agencias",
                newName: "Agencia");

            migrationBuilder.RenameIndex(
                name: "IX_VisitaTareas_VisitaID",
                table: "VisitaTarea",
                newName: "IX_VisitaTarea_VisitaID");

            migrationBuilder.RenameIndex(
                name: "IX_Visitas_UsuarioID",
                table: "Visita",
                newName: "IX_Visita_UsuarioID");

            migrationBuilder.RenameIndex(
                name: "IX_Visitas_EstadoVisitaID",
                table: "Visita",
                newName: "IX_Visita_EstadoVisitaID");

            migrationBuilder.RenameIndex(
                name: "IX_Visitas_CasoID",
                table: "Visita",
                newName: "IX_Visita_CasoID");

            migrationBuilder.RenameIndex(
                name: "IX_Regiones_TipoRegionID",
                table: "Region",
                newName: "IX_Region_TipoRegionID");

            migrationBuilder.RenameIndex(
                name: "IX_Regiones_PadreID",
                table: "Region",
                newName: "IX_Region_PadreID");

            migrationBuilder.RenameIndex(
                name: "IX_Clientes_RegionID",
                table: "Cliente",
                newName: "IX_Cliente_RegionID");

            migrationBuilder.RenameIndex(
                name: "IX_Clientes_GeneroID",
                table: "Cliente",
                newName: "IX_Cliente_GeneroID");

            migrationBuilder.RenameIndex(
                name: "IX_Clientes_EstadoCivilID",
                table: "Cliente",
                newName: "IX_Cliente_EstadoCivilID");

            migrationBuilder.RenameIndex(
                name: "IX_Casos_ClienteID",
                table: "Caso",
                newName: "IX_Caso_ClienteID");

            migrationBuilder.RenameIndex(
                name: "IX_Casos_CategoriaID",
                table: "Caso",
                newName: "IX_Caso_CategoriaID");

            migrationBuilder.RenameIndex(
                name: "IX_Agencias_RegionID",
                table: "Agencia",
                newName: "IX_Agencia_RegionID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VisitaTarea",
                table: "VisitaTarea",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Visita",
                table: "Visita",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipoRegion",
                table: "TipoRegion",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Region",
                table: "Region",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Genero",
                table: "Genero",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EstadoVisita",
                table: "EstadoVisita",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cliente",
                table: "Cliente",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Categoria",
                table: "Categoria",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Caso",
                table: "Caso",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Agencia",
                table: "Agencia",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Agencia_Region_RegionID",
                table: "Agencia",
                column: "RegionID",
                principalTable: "Region",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Caso_Categoria_CategoriaID",
                table: "Caso",
                column: "CategoriaID",
                principalTable: "Categoria",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Caso_Cliente_ClienteID",
                table: "Caso",
                column: "ClienteID",
                principalTable: "Cliente",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cliente_EstadoCivil_EstadoCivilID",
                table: "Cliente",
                column: "EstadoCivilID",
                principalTable: "EstadoCivil",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cliente_Genero_GeneroID",
                table: "Cliente",
                column: "GeneroID",
                principalTable: "Genero",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cliente_Region_RegionID",
                table: "Cliente",
                column: "RegionID",
                principalTable: "Region",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Region_Region_PadreID",
                table: "Region",
                column: "PadreID",
                principalTable: "Region",
                principalColumn: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Region_TipoRegion_TipoRegionID",
                table: "Region",
                column: "TipoRegionID",
                principalTable: "TipoRegion",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuarios_Agencia_AgenciaID",
                table: "Usuarios",
                column: "AgenciaID",
                principalTable: "Agencia",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visita_Caso_CasoID",
                table: "Visita",
                column: "CasoID",
                principalTable: "Caso",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visita_EstadoVisita_EstadoVisitaID",
                table: "Visita",
                column: "EstadoVisitaID",
                principalTable: "EstadoVisita",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visita_Usuarios_UsuarioID",
                table: "Visita",
                column: "UsuarioID",
                principalTable: "Usuarios",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VisitaTarea_Visita_VisitaID",
                table: "VisitaTarea",
                column: "VisitaID",
                principalTable: "Visita",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
