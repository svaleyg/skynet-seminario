﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace seminario.api.Migrations
{
    public partial class FuncionalidadPadreNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Funcionalidades_Funcionalidades_PadreID",
                table: "Funcionalidades");

            migrationBuilder.AlterColumn<int>(
                name: "PadreID",
                table: "Funcionalidades",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Funcionalidades_Funcionalidades_PadreID",
                table: "Funcionalidades",
                column: "PadreID",
                principalTable: "Funcionalidades",
                principalColumn: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Funcionalidades_Funcionalidades_PadreID",
                table: "Funcionalidades");

            migrationBuilder.AlterColumn<int>(
                name: "PadreID",
                table: "Funcionalidades",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Funcionalidades_Funcionalidades_PadreID",
                table: "Funcionalidades",
                column: "PadreID",
                principalTable: "Funcionalidades",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
