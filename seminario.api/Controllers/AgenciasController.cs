﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Modelo;
using seminario.api.Negocio;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AgenciasController : ControllerBase
    {
        private readonly NAgencias _agencias;
        public AgenciasController(NAgencias agencias)
        {
            _agencias = agencias;
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerAgencias()
        {
            var users = await _agencias.ObtenerAgencias();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ObtenerAgencia(int id)
        {
            var users = await _agencias.ObtenerAgencia(id);
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> CrearAgencia(InputAgencia request)
        {
            var resultado = await _agencias.CrearAgencia(request);
            return Ok(resultado);
        }

        [HttpPut]
        public async Task<IActionResult> EditarAgencia(InputAgencia request)
        {
            var resultado = await _agencias.EditarAgencia(request);
            return Ok(resultado);
        }

        [HttpPut("CambiarEstado/{id}")]
        public async Task<IActionResult> CambiarEstadoAgencia(int id)
        {
            var resultado = await _agencias.CambiarEstadoAgencia(id);
            return Ok(resultado);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> EliminarAgencia(int id)
        {
            var resultado = await _agencias.EliminarAgencia(id);
            return Ok(resultado);
        }
    }
}
