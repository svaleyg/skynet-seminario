﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Modelo;
using seminario.api.Negocio;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CasosController : ControllerBase
    {
        private readonly NCasos _casos;
        public CasosController(NCasos casos)
        {
            _casos = casos;
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerCasos()
        {
            var users = await _casos.ObtenerCasos();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ObtenerCaso(int id)
        {
            var users = await _casos.ObtenerCaso(id);
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> CrearCaso(InputCaso request)
        {
            var resultado = await _casos.CrearCaso(request);
            return Ok(resultado);
        }

        [HttpPut]
        public async Task<IActionResult> EditarCaso(InputCaso request)
        {
            var resultado = await _casos.EditarCaso(request);
            return Ok(resultado);
        }

        [HttpPost("CrearVisita")]
        public async Task<IActionResult> CrearVisitaCaso(InputCrearVisita request)
        {
            var resultado = await _casos.CrearVisitaCaso(request);
            return Ok(resultado);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> EliminarCaso(int id)
        {
            var resultado = await _casos.EliminarCaso(id);
            return Ok(resultado);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> CerrarCaso(int id)
        {
            var resultado = await _casos.CerrarCaso(id);
            return Ok(resultado);
        }
    }
}
