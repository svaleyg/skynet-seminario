﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Modelo;
using seminario.api.Negocio;
using seminario.api.Utilidades;
using System.Security.Claims;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly NClientes _clientes;
        public ClientesController(NClientes clientes)
        {
            _clientes = clientes;
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerClientes()
        {
            var users = await _clientes.ObtenerClientes();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ObtenerCliente(int id)
        {
            var users = await _clientes.ObtenerCliente(id);
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> CrearCliente(InputCliente request)
        {
            var resultado = await _clientes.CrearCliente(request);
            return Ok(resultado);
        }

        [HttpPut]
        public async Task<IActionResult> EditarCliente(InputCliente request)
        {
            var resultado = await _clientes.EditarCliente(request);
            return Ok(resultado);
        }

        [HttpPut("CambiarEstado/{id}")]
        public async Task<IActionResult> CambiarEstadoCliente(int id)
        {
            var resultado = await _clientes.CambiarEstadoCliente(id);
            return Ok(resultado);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> EliminarCliente(int id)
        {
            var resultado = await _clientes.EliminarCliente(id);
            return Ok(resultado);
        }
    }
}
