﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Modelo;
using seminario.api.Negocio;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VisitasController : ControllerBase
    {
        private readonly NVisitas _visitas;
        public VisitasController(NVisitas visitas)
        {
            _visitas = visitas;
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerVisitas()
        {
            var users = await _visitas.ObtenerVisitas();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ObtenerVisita(int id)
        {
            var users = await _visitas.ObtenerVisita(id);
            return Ok(users);
        }

        [HttpPost("Iniciar")]
        public async Task<IActionResult> IniciarVisita(InputIniciarVisita request)
        {
            var users = await _visitas.IniciarVisita(request);
            return Ok(users);
        }

        [HttpPost("AgregarTareasVisita")]
        public async Task<IActionResult> AgregarTareasVisita(InputFinalizarVisita request)
        {
            var users = await _visitas.AgregarTareasVisita(request);
            return Ok(users);
        }

        [HttpPost("Finalizar")]
        public async Task<IActionResult> FinalizarVisita(InputFinalizarVisita request)
        {
            var users = await _visitas.FinalizarVisita(request);
            return Ok(users);
        }

        [HttpPut("Cancelar/{id}")]
        public async Task<IActionResult> CancelarVisita(int id)
        {
            var resultado = await _visitas.CancelarVisita(id);
            return Ok(resultado);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> EliminarVisita(int id)
        {
            var resultado = await _visitas.EliminarVisita(id);
            return Ok(resultado);
        }
    }
}
