﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Negocio;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionalidadesController : ControllerBase
    {
        private readonly NFuncionalidades _funcionalidades;
        public FuncionalidadesController(NFuncionalidades funcionalidades)
        {
            _funcionalidades = funcionalidades;
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerFuncionalidades()
        {
            var users = await _funcionalidades.ObtenerFuncionalidades();
            return Ok(users);
        }
    }
}
