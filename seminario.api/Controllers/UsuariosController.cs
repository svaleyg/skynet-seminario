﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Modelo;
using seminario.api.Negocio;
using seminario.api.Utilidades;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly NUsuarios _usuarios;
        public UsuariosController(NUsuarios usuarios)
        {
            _usuarios = usuarios;
        }

        [AllowAnonymous]
        [HttpPost("~/api/Login")]
        public async Task<IActionResult> Login(InputUsuario usuario)
        {
            if (string.IsNullOrWhiteSpace(usuario.Usuario) || string.IsNullOrWhiteSpace(usuario.Contrasenia))
                throw new AppException("Debe de ingresar su Usuario y Contraseña", System.Net.HttpStatusCode.Unauthorized);

            var user = await _usuarios.Login(usuario.Usuario, usuario.Contrasenia);
            return Ok(user);
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerUsuarios()
        {
            var users = await _usuarios.ObtenerUsuarios();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ObtenerUsuarios(int id)
        {
            var users = await _usuarios.ObtenerUsuario(id);
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> CrearUsuario(InputUsuarioCompleto request)
        {
            var resultado = await _usuarios.CrearUsuario(request);
            return Ok(resultado);
        }

        [HttpPut]
        public async Task<IActionResult> EditarUsuario(InputUsuarioCompleto request)
        {
            var resultado = await _usuarios.EditarUsuario(request);
            return Ok(resultado);
        }

        [HttpPut("CambiarEstado/{id}")]
        public async Task<IActionResult> CambiarEstadoUsuario(int id)
        {
            var resultado = await _usuarios.CambiarEstadoUsuario(id);
            return Ok(resultado);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> EliminarUsuario(int id)
        {
            var resultado = await _usuarios.EliminarUsuario(id);
            return Ok(resultado);
        }
    }
}
