﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using seminario.api.Modelo;
using seminario.api.Negocio;

namespace seminario.api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogosController : ControllerBase
    {
        private readonly NCatalogos _catalogos;

        public CatalogosController(NCatalogos catalogos)
        {
            _catalogos = catalogos;
        }

        [HttpGet("{opcion}")]
        public async Task<IActionResult> Login(int opcion)
        {
            var user = await _catalogos.ObtenerCatalogos(opcion);
            return Ok(user);
        }
    }
}
