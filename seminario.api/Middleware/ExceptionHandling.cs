﻿using seminario.api.Modelo;
using seminario.api.Utilidades;
using System.Net;
using System.Text.Json;

namespace seminario.api.Middleware
{
    public class ExceptionHandling
    {
        private readonly RequestDelegate _next;

        public ExceptionHandling(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            var response = context.Response;

            string mensaje = string.Empty;

            switch (exception)
            {
                case AppException ex:
                    response.StatusCode = (int)ex.StatusCode;
                    mensaje = ex.Message;
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    mensaje = exception.Message;
                    break;
            }

            var errorResponse = new OutputResultado
            {
                Resultado = false,
                Mensaje = mensaje
            };

            var result = JsonSerializer.Serialize(errorResponse);
            await context.Response.WriteAsync(result);
        }
    }
}
