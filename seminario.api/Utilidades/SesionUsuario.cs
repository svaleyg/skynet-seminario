﻿using seminario.api.Modelo;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace seminario.api.Utilidades
{
    public interface ISesionUsuario
    {
        User ObtenerSesionUsuario();
    }

    public class SesionUsuario : ISesionUsuario
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SesionUsuario(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public User ObtenerSesionUsuario()
        {
            var claimsIdentity = _httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;

            if (claimsIdentity == null)
            {
                return null;
            }

            var usuario = new User
            {
                ID = Convert.ToInt32(claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("ID"))?.Value),
                Usuario = claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("Usuario"))?.Value,
                Nombre = claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("Nombre"))?.Value,
                Correo = claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("Correo"))?.Value,
                Agencia = new()
                {
                    ID = Convert.ToInt32(claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("AgenciaID"))?.Value)
                },
                FotoPerfil = claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("FotoPerfil"))?.Value,
                Rol = new()
                {
                    Nombre = claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals("Rol"))?.Value
                }
            };

            return usuario;
        }
    }
}
