﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using seminario.api.Modelo;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace seminario.api.Utilidades
{
    public class GeneradorToken
    {
        private readonly IOptions<AppSettings> _appSettings;

        public GeneradorToken(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings;
        }

        public string GenerarTokenJWT(User usuario)
        {
            // CREAMOS EL HEADER //
            var _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Value.JWT.ClaveSecreta));
            var _signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var _Header = new JwtHeader(_signingCredentials);

            var _Claims = new[] {
                new Claim("ID", usuario.ID.ToString()),
                new Claim("Usuario", usuario.Usuario),
                new Claim("Nombre", usuario.Nombre),
                new Claim("Correo", usuario.Correo),
                new Claim("AgenciaID", usuario.Agencia.ID.ToString()),
                //new Claim("FotoPerfil", usuario.FotoPerfil),
                new Claim("Rol", usuario.Rol.Nombre)
            };

            var _Payload = new JwtPayload(
                issuer: _appSettings.Value.JWT.Issuer,
                audience: _appSettings.Value.JWT.Audience,
                claims: _Claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(_appSettings.Value.TiempoExpiracion)
                );

            var _Token = new JwtSecurityToken(_Header, _Payload);

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }
    }
}
