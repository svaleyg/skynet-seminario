﻿using System.Net;

namespace seminario.api.Utilidades
{
    public class AppException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public AppException(string mensaje, HttpStatusCode statusCode) : base(mensaje)
        {
            StatusCode = statusCode;
        }
    }
}
