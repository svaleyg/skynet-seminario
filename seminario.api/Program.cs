using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using seminario.api.Middleware;
using seminario.api.Modelo;
using seminario.api.Negocio;
using seminario.api.Persistencia;
using seminario.api.Utilidades;
using System.Text;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

IConfiguration configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json")
        .Build();

// Add services to the container.
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.IgnoreNullValues = true;
});

string connectionString = configuration["ConnectionStrings:ConexionDatabase"];

builder.Services.AddDbContext<ContextSeminario>(options =>
{
    options.UseNpgsql(connectionString);
});

builder.Services.AddControllers()
    .AddJsonOptions(x =>
    x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<ISesionUsuario, SesionUsuario>();
builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<NUsuarios>();
builder.Services.AddScoped<NClientes>();
builder.Services.AddScoped<NCatalogos>();
builder.Services.AddScoped<NCasos>();
builder.Services.AddScoped<NVisitas>();
builder.Services.AddScoped<NFuncionalidades>();
builder.Services.AddScoped<NAgencias>();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration["JWT:Issuer"],
            ValidAudience = configuration["JWT:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(configuration["JWT:ClaveSecreta"])
            )
        };
    });

builder.Services.Configure<AppSettings>(configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseMiddleware<ExceptionHandling>();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
