﻿using Microsoft.EntityFrameworkCore;
using seminario.api.Modelo;

namespace seminario.api.Persistencia
{
    public class ContextSeminario : DbContext
    {
        public ContextSeminario(DbContextOptions<ContextSeminario> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }

        public DbSet<Rol> Roles { get; set; }
        public DbSet<Funcionalidad> Funcionalidades { get; set; }
        public DbSet<RolFuncionalidad> RolFuncionalidades { get; set; }
        public DbSet<User> Usuarios { get; set; }
        public DbSet<Agencia> Agencias { get; set; }
        public DbSet<Region> Regiones { get; set; }
        public DbSet<TipoRegion> TipoRegiones { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Genero> Generos { get; set; }
        public DbSet<EstadoCivil> EstadoCivil { get; set; }
        public DbSet<Caso> Casos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Visita> Visitas { get; set; }
        public DbSet<EstadoVisita> EstadoVisitas { get; set; }
        public DbSet<VisitaTarea> VisitaTareas { get; set; }
    }
}
